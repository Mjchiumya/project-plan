<?php

class adminModel extends CI_Model{

	  function __construct(){
        parent::__construct();
        $this->load->database();
	}

public function insertToBusiness($data){

       $this->db->insert('business',$data);
}

public function insertToService($data){
       $this->db->insert('service',$data);
       return true;
}

public function servicePieData(){
		 $name = $this->db->count_all_results('service');

		  $this->db->select('s_demography');
		   $this->db->from('service');
		   $this->db->distinct();
		   $result = $this->db->get();
		   $demography=count($result->result());
		   
		  $this->db->select('s_category');
		   $this->db->from('service');
		   $this->db->distinct();
		   $items = $this->db->get();
		   $category=count($items->result());

		   return $data = array($name,$demography,$category);
	}
	public function usersCount(){
		            $allCounts = array(); 	   	   
		    
		      	    $result = $this->db->count_all_results("user");
		     	    $allCounts['All'] = $result;
                    
		     	    $this->db->where('u_password', 'deactivated');
                    $this->db->from('user');
                    $deactivated = $this->db->count_all_results();
                    $allCounts['deactivated'] = $deactivated;

                    $this->db->where('u_password !=', 'deactivated');
                    $this->db->from('user');
                    $active = $this->db->count_all_results();
                    $allCounts['active'] = $active;                    
                    
                    $sevenDaysago = date('Y-m-d', strtotime('-7'));
                    $this->db->where('created >',$sevenDaysago);                    
                    $this->db->from('user');
                    $recent = $this->db->count_all_results();
                    $allCounts['recent'] = $recent;

		            return $allCounts;
	}
	public function serviceCount(){
		            $allCounts = array(); 	   	   
		            
		      	    $result = $this->db->count_all_results("service");
		     	    $allCounts['All'] = $result;
                    
		     	    $this->db->select('s_category');
                    $this->db->from('service');
                    $category = $this->db->count_all_results();
                    $allCounts['categories'] = $category;

                    $this->db->select('s_demography');
                    $this->db->from('service');
                    $area = $this->db->count_all_results();
                    $allCounts['areas'] = $area;

                    $this->db->select('s_status');
                    $this->db->from('service');
                    $this->db->where('s_status',1);
                    $active = $this->db->count_all_results();
                    $allCounts['active'] = $active; 

                    $this->db->select('s_status');
                    $this->db->from('service');
                     $this->db->where('s_status',0);
                    $inactive = $this->db->count_all_results();
                    $allCounts['inactive'] = $inactive;                 
                    
		            return $allCounts;
	}
	public function businessCount(){
		            $allCounts = array(); 	   	   
		    
		      	    $result = $this->db->count_all_results("business");
		     	    $allCounts['All'] = $result;
                    
		     	    $this->db->select('b_location');
                    $this->db->from('business');
                    $location = $this->db->count_all_results();
                    $allCounts['location'] = $location;

		            return $allCounts;
	}
}
