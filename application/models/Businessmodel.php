<?php

class BusinessModel extends CI_Model{

	 function __construct(){
        parent::__construct();
        $this->load->database();
	}  

     public function getNameId() {
        $this->db->select('b_name,b_id');
         $this->db->distinct('b_name');
		   $this->db->limit(10);
        $query = $this->db->get('business');
        return $query->result();
    }
    
    public function getById($bid) {
        $this->db->select('*');
        $this->db->where('b_id',$bid);        
        $query = $this->db->get('business');
        return $query->result();
    }

    public function getBySid($sid) {
        $this->db->select('*');
         $this->db->where('s_id',$sid);
        $query = $this->db->get('services');
        return $query->result_array();
    } 

    public function all(){       
        $query = $this->db->get('business');
        $service_array = $query->result_array();
        return $service_array;
    }

    public function update($bid,$data){
     $this->db->where('b_id',$bid);
        $this->db->update('business',$data);
        return true;
    }

    public function getEmail($data){         
      $this->db->select('b_email');
        foreach ($data as $key => $value) {
            $this->db->or_like('b_id',$value);
        }      
         $query = $this->db->get('business');
         return $query->result();
    }

    public function uploadLogo($image,$bid){
       $data = array('b_img' => $image);
       $this->db->where('b_id',$bid);
       $this->db->update('business',$data);
        return true;
    }

   public function destroy($bid){
     $this->db->where('b_id',$bid);
        $this->db->delete('business');
        return true;
    }

}