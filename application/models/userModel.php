<?php

class userModel extends CI_Model{

	  function __construct(){
        parent::__construct();
        $this->load->database();
	}

public function counts() {
      return $this->db->count_all("user");
}

public function getAll(){
  $this->db->select('u_name,u_mobile,u_email,u_status');
  $query = $this->db->get('user');
  return $query->result();
}

public function get_user($uid) {
   $this->db->select('*');
     $this->db->where('u_id',$uid);
      $query = $this->db->get('user');
        return $query->result();
      }

public function get_emailDetails($uid) {
   $this->db->select('u_name,u_mobile,u_email');
     $this->db->where('u_id',$uid);
      $query = $this->db->get('user');
        return $query->result();
      }

public function getId($email){
   $this->db->select('u_id');
     $this->db->where('u_email',$email);
      $this->db->from('user');
       $reault_array = $this->db->get()->result_array();
return $reault_array[0]['u_id'];
    } 

public function updatePassword($id,$pass){
    $this->db->set('u_password',md5($pass));
   $this->db->where('u_id',$id);
    $this->db->update('user');
    return true;
     }
}	