<?php

class Mymodel extends CI_Model{

	 function __construct(){
        parent::__construct();
        $this->load->database();
	}

public function all_events(){
    	 $query = $this->db->get('event');
       $event_array = $query->result_array();
       return $event_array;
    }

public function load_db_cart($eid){
    $this->db->select('*');
     $this->db->from('eventprofile');
      $this->db->where('eid',$eid);
       $this->db->join('service','eventprofile.sid = service.s_id');
        $this->db->join('business','eventprofile.bid = business.b_id');
       $query = $this->db->get();
       if ($query != null) {
      return $query->result_array();
    }
    return false;
}

public function serviceOrder($eid){
    $this->db->select("*");
	 $this->db->from('eventprofile');
	  $this->db->where('eid',$eid);
	   $query = $this->db->get();
	     if($query->num_rows() > 0){
	        $this->db->set('e_status','created');
	         $this->db->where('e_id',$eid);
	          $this->db->update('event');
			  return true;
	   }else if($query->num_rows() <= 0){
	              return false;
	                    }

	 return false;
}

public function orderCancel($eid){
	$this->db->set('e_status','cancelled');
	$this->db->where('e_id',$eid);
	$this->db->update('event');
}
public function cartdbmode($data){
  $this->db->insert('eventprofile', $data);        
     }
}
