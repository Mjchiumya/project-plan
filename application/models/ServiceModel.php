<?php
class ServiceModel extends CI_Model{

  function __construct(){
        parent::__construct();
        $this->load->database();
	}


public function counts() {
      return $this->db->count_all("service");
}

public function all($limit='',$start='') {
	 $this->db->select("*");
     $this->db->where('s_status',1);

	   if($limit === null && $start === null){
         $query = $this->db->get("service");
          return $query->result();
	     }
	     else{        
            $this->db->limit($limit, $start);
              $query = $this->db->get("service");

                if ($query->num_rows() > 0) {
                    foreach ($query->result() as $row) {
                       $data[] = $row;
                    }
                     return $data;
                   }

                 return false;
    }
}

public function getSix() {
   $this->db->select('*');
   $this->db->where('s_status',1);
   $this->db->limit(6);
     $query = $this->db->get('service');
         return $query->result();
}

public function getBusiness($bid) {
   $this->db->select('*');
   $this->db->where('b_id',$bid);
      $query = $this->db->get('service');
        return $query->result();
}


public function filterByCategory($limit,$start,$category) {
        $this->db->select("*");
        $this->db->where(array('s_status'=>1,'s_category'=>$category));        
        $this->db->limit($limit, $start);
        $query = $this->db->get("service");

        if ($query->num_rows() > 0) {

            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
} 

public function filterByAll($limit,$start,$category,$location) {
        $this->db->select("*");
      
        $this->db->where(array('s_category'=>$category,'s_demography'=>$location,'s_status'=>1));
        $this->db->limit($limit, $start);
        $query = $this->db->get("service");

        if ($query->num_rows() > 0) {

            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
}

public function upDateImage($data,$sid){
        $this->db->where('s_id',$sid);
        $this->db->update('service',$data);
        return true;
}

public function filterByLocation($limit,$start,$location) {
        $this->db->select("*");
        $this->db->where('s_demography',$location);
        $this->db->limit($limit, $start);
        $query = $this->db->get("service");

        if ($query->num_rows() > 0) {

            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
}

public function allCategories() {
        $this->db->select('s_category');
        $this->db->where('s_status',1);
         $this->db->distinct('s_category');		  
         $query = $this->db->get('service');
         return $query->result();
    }

public function getTenCategories() {
        $this->db->select('s_category');
        $this->db->where('s_status',1);
         $this->db->distinct('s_category');
		  $this->db->limit(10);
        $query = $this->db->get('service');
        return $query->result();
    }

public function getTenDemography() {
        $this->db->select('s_demography');
        $this->db->where('s_status',1);
         $this->db->distinct('s_demography');
		    $this->db->limit(10);
          $query = $this->db->get('service');
        return $query->result();
    }
public function allDemography() {
        $this->db->select('s_demography');
        $this->db->where('s_status',1);
         $this->db->distinct('s_demography');		 
          $query = $this->db->get('service');
        return $query->result();
    }

public function getById($sid) {
        $this->db->select('*');
         $this->db->where('s_id',$sid);
        $query = $this->db->get('service');
        return $query->result();
    }

public function getByBid($bid) {
        $this->db->select('*');
         $this->db->where('b_id',$bid);
        $query = $this->db->get('service');
        return $query->result_array();
    } 

public function getActive($bid) {
        $this->db->select('*');
         $this->db->where('b_id',$bid);
         $this->db->where('s_status',1);
        $query = $this->db->get('service');
        return $query->result();
    } 

public function getInactive($bid) {
        $this->db->select('*');
         $this->db->where('b_id',$bid);
         $this->db->where('s_status',0);
        $query = $this->db->get('service');
        return $query->result();
    }

public function getNameId(){
       $this->db->select('s_name,s_id');
       $this->db->where('s_status',1);
         $this->db->distinct('s_name');
		  $this->db->limit(10);
        $query = $this->db->get('service');
        return $query->result();
}

public function update($sid,$data){
     $this->db->where('s_id',$sid);
        $this->db->update('service',$data);
        return true;
}

public function activateStatus($sid){ 
   $this->db->set('s_status',1);   
    $this->db->where('s_id',$sid);
      $this->db->update('service');
        return true;
}

public function deactivateStatus($sid){ 
   $this->db->set('s_status',0);   
    $this->db->where('s_id',$sid);
      $this->db->update('service');
        return true;
}

//get service by id
public function get_services($eid) {
   $this->db->select('*');
     $this->db->where('eid',$eid);
      $query = $this->db->get('eventprofile');
        return $query->result();
}

public function destroy($sid){
     $this->db->where('s_id',$sid);
     $this->db->delete('service');
        return true;
}

public function destroyAll($bid){
     $this->db->where('b_id',$bid);
     $this->db->delete('service');
        return true;
}


}