<?php

/**
 * AuthModel short summary.
 *
 * AuthModel description.
 *
 * @version 1.0
 * @author acer
 */
class AuthModel extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->load->database();
	}

    //register user
    public function register_user($user){
        $this->db->insert('user', $user);
    }
    //login user
    public function login_user($email,$pass){
        $this->db->select('u_email,u_name,u_id');
        $this->db->from('user');
        $this->db->where(array('u_email'=>$email,'u_password'=>$pass,'u_status'=>1));   
        if($query=$this->db->get())
        {
            return $query->row_array();
        }
        else{
            return false;
        }
    }
    //check user email if unique
    public function email_check($email){
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where(array('u_email'=>$email,'u_status'=>1));
        $query=$this->db->get();
        if($query->num_rows()>0){
            return false;
        }else{
            return true;
        }
    }
    //deactivate account
    public function account_deactivate($uid){
        $this->db->set('u_status',0);
        $this->db->where('u_id',$uid);
        $this->db->update('user');
        return true;
    }


}
