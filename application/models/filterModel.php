<?php

class filterModel extends CI_Model{ 

    public function __construct() {
      parent::__construct(); 
       $this->load->database();
       }
   
   public function countBusiness(){
     return $this->db->count_all_results('business');
   }

   public function getAllBusiness($limit,$start,$search){ 

       if ($search === '' ) {
            $this->db->select("*");
             $this->db->order_by('b_id','desc');
             $this->db->limit($limit, $start);
              $query = $this->db->get("business");            
               return $query;
         }
      else{
            $this->db->select("*");
            $this->db->like('b_name', $search);
             $this->db->or_like('b_location', $search);
             $this->db->order_by('b_id','desc');           
             $this->db->limit($limit, $start);
              $query = $this->db->get("business");            
               return $query;
             }
}

public function getAllService($limit,$start,$search){ 

       if ($search === '' ) {
            $this->db->select("*");
             $this->db->order_by('s_id','desc');
             $this->db->limit($limit, $start);
              $query = $this->db->get("service");            
               return $query;
         }
      else{
            $this->db->select("*");
            $this->db->like('s_name', $search);
             $this->db->or_like('s_category', $search);
             $this->db->or_like('s_demography', $search);
             $this->db->order_by('s_id','desc');           
             $this->db->limit($limit, $start);
              $query = $this->db->get("service");            
               return $query;

         }      
   }
    


      
}
 


