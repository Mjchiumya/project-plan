<?php

/**
 * EventModel short summary.
 *
 * EventModel description.
 *
 * @version 1.0
 * @author acer
 */
class EventModel extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->load->database();
	}

    //count orders 
    public function ordercount() {
      return $this->db->count_all("orders");
   }

   public function ordering($eid,$uid){
     $this->db->insert('orders',array('e_id'=>$eid,'u_id'=>$uid));
   }

    //insert into event table
    public function insert_toevt($data){
        $this->db->insert('event',$data);
    }

    //update event table
    public function update_toevt($data,$eid){
        $this->db->where('e_id',$eid);
        $this->db->update('event',$data);
        return true;
    }

    //read created events
    public function readEvent($uid){
        $this->db->select('*');
        $this->db->from('event');
        $this->db->where(array('uid' => $uid,'e_status'=>'created'));
        $query = $this->db->get();
        return $query->result();
    }
    //read user's unique events
    public function read_event_with_id($uid,$eid){
        $this->db->select('*');
        $this->db->from('event');
        $this->db->where(array('uid' => $uid,'e_id'=>$eid));
        $query = $this->db->get();
        return $query->result();
    }
    //read draft events
    public function readDraftEvent($uid){
        $this->db->select('*');
        $this->db->from('event');
        $this->db->where(array('uid' => $uid,'e_status'=>'draft'));
        $query = $this->db->get();
        return $query->result();
    }
    //read cancelled events
    public function readCancelledEvent($uid){
        $this->db->select('*');
        $this->db->from('event');
        $this->db->where(array('uid' => $uid,'e_status'=>'cancelled'));
        $query = $this->db->get();
        return $query->result();
    }
    //read delete eventprofile
    public function del_evtprofile($eid){
        if ( $this->db->delete( 'eventprofile',array( 'eid'=>$eid ) ) ) {
            return true;
        }else{
            return false;
        }
    }
    //read delete event
    public function del_event_mode($eid,$uid){
        $this->db->delete('eventprofile',array('eid'=>$eid));
         $this->db->delete('event',array('e_id'=>$eid,'uid'=>$uid));
            return true; 
    }
    
    public function get_emailBody($eid){       
        $this->db->select('*');        
         $this->db->where(array('eid'=>$eid));
         $query = $this->db->get('eventprofile');
         return $query->result();
    }
    public function getBid($eid){
        $this->db->select('bid');        
         $this->db->where(array('eid'=>$eid));
         $query = $this->db->get('eventprofile');
         return $query->result();
    }
    public function getOrderDetails(){
        $this->db->select('orders.o_id,orders.u_id,orders.e_id,event.e_name,event.e_date,event.e_status,user.u_name');
          $this->db->from('orders'); 
             $this->db->join('user','user.u_id = orders.u_id');           
             $this->db->join('event','event.e_id = orders.e_id');
               $query = $this->db->get();
                 return $query->result();
    }

}
