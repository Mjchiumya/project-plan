<?php

/**
 * EventController short summary.
 *
 * EventController description.
 *
 * @version 1.0
 * @author acer
 */
class EventController extends CI_Controller{

    //constructor
    public function __construct(){
		parent::__construct();
        $this->load->model(array('EventModel','Mymodel'));
        $this->load->library(array('cart','session','form_validation'));
        $this->load->helper(array('form','url'));
    }

    //show create event form
    public function event_input(){
        if(isset($this->session->user_id)){
           $this->load->view("temps/header");
            $this->load->view("temps/menu");
            $this->load->view("pages/eventinputform");           
           }else{
            redirect('login');
        }
    }

    //event view
    public function eventview($eid){
        $uid = intval($this->session->user_id);
        $data['event'] = $this->EventModel->read_event_with_id($uid,$eid);
        $data['products'] =$this->Mymodel->load_db_cart($eid);
        $data['eid'] = array('eid'=>$eid);
          $this->load->view('temps/header');
          $this->load->view('temps/menu');          
          $this->load->view('pages/eventview',$data);
}

    //event editor form
    public function event_editor($eid){
        $uid = intval($this->session->user_id);
        $data['event'] = $this->EventModel->read_event_with_id($uid,$eid);
        $data['eid'] = array('eid'=>$eid);
        $this->load->view("temps/header");
        $this->load->view('temps/menu');
        $this->load->view("pages/eventeditor",$data);
        $this->load->view("temps/footer");
    }

    //live event view
    public function liveEventView($eid){
        $uid = intval($this->session->user_id);
        $data['event'] = $this->EventModel->read_event_with_id($uid,$eid);
        $data['products'] =$this->Mymodel->load_db_cart($eid);
        $data['eid'] = array('eid'=>$eid);
        $this->load->view('temps/header');
        $this->load->view("temps/menu");
        $this->load->view('pages/liveeventview',$data);
    }
    
    //delete event
    public function del_event($eid){
        $uid = intval($this->session->user_id);
        if ($this->EventModel->del_event_mode($eid,$uid)) {
            redirect('user','refresh');
        }else{
           
        }
    }

  
   public function event_form(){
    $this->form_validation->set_rules('organiser','organiser', 'required|min_length[2]');
    $this->form_validation->set_rules('name','name', 'required');
     $this->form_validation->set_rules('evtdate','evtdate', 'required');
      $this->form_validation->set_rules('evtlocation','Evtlocation', 'required');
       $this->form_validation->set_rules('evttime','evttime', 'required');
        $this->form_validation->set_rules('evttype','evttype', 'required');
        $this->form_validation->set_rules('evtcharge','evtcharge', 'required');
        $this->form_validation->set_rules('evtmsg','evtmsg', 'required');

        $organiser = $this->input->post('organiser');
        $name = $this->input->post('name');
        $evtdate = $this->input->post('evtdate');
        $evtlocation = $this->input->post('evtlocation');
        $evttime = $this->input->post('evttime');
        $evttype = $this->input->post('evttype');
        $evtmsg = $this->input->post('evtmsg');
        $evtcharge = $this->input->post('evtcharge');
        $uid = $this->session->user_id;
    if ($this->form_validation->run() == FALSE){
              echo $this->event_input();
                }else
                   {
                       $data  = array(
                         'e_name' => $name,
                          'e_organiser'=>$organiser,
                           'e_date'=>$evtdate,
                            'e_time'=>$evttime,
                              'e_location'=>$evtlocation,
                          'e_type'=>$evttype,
                          'e_notes'=>$evtmsg,
                          'e_charge'=>floatval($evtcharge),
                          'uid' =>$uid,
                          'e_status'=>'draft'
                            );

                       if (isset($data)) {
                        $this->EventModel->insert_toevt($data);
                       }

                     redirect('user', 'refresh');
                   }
}

public function evtedit_formval(){
  $this->form_validation->set_rules('organiser','organiser', 'required');
    $this->form_validation->set_rules('name','name', 'required');
     $this->form_validation->set_rules('evtdate','evtdate', 'required');
      $this->form_validation->set_rules('evtlocation','evtlocation','required');
       $this->form_validation->set_rules('evttime','evttime', 'required');
        $this->form_validation->set_rules('evttype','evttype', 'required');
        $this->form_validation->set_rules('evtcharge','evtcharge', 'required');
        $this->form_validation->set_rules('evtmsg','evtmsg', 'required');

        $organiser = $this->input->post('organiser');
        $name = $this->input->post('name');
        $evtdate = $this->input->post('evtdate');
        $evtlocation = $this->input->post('evtlocation');
        $evttime = $this->input->post('evttime');
        $evttype = $this->input->post('evttype');
        $evtmsg = $this->input->post('evtmsg');
        $evtcharge = $this->input->post('evtcharge');
        $eid=intval($this->input->post('id'));


    if ($this->form_validation->run() == false){
        $uid = intval($this->session->user_id);
        $data['event'] = $this->EventModel->read_event_with_id($uid,$eid);
        $data['eid'] = array('eid'=>$eid);
        $this->load->view("temps/header");
        $this->load->view('temps/menu');
        $this->load->view("pages/eventeditor",$data);
        $this->load->view("temps/footer");

       }else{
           $data  = array(
                         'e_name' => $name,
                          'e_organiser'=>$organiser,
                           'e_date'=>$evtdate,
                            'e_time'=>$evttime,
                              'e_location'=>$evtlocation,
                          'e_type'=>$evttype,
                          'e_notes'=>$evtmsg,
                          'e_charge'=>floatval($evtcharge)
                            );

                       if (isset($data)) {
                        $this->EventModel->update_toevt($data,$eid);
                       }

                     redirect('user');
                   }
      }
 

}
