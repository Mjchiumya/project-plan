<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * pagesController short summary.
 *
 * pagesController description.
 *
 * @version 1.0
 * @author acer
 */
class PagesController extends CI_Controller{

    //constructor
public function __construct(){
	 parent::__construct();
      $this->load->model(array('Mymodel','EventModel','UserModel','BusinessModel','ServiceModel'));
      $this->load->library(array('cart','session','form_validation'));
      $this->load->helper(array('form','url'));
    }

    
public function index(){
    $data['products']=$this->ServiceModel->getNameId();
    $data['services'] =$this->ServiceModel->getSix();
    $data['events']=$this->Mymodel->all_events();
    $data['categories']=$this->ServiceModel->getTenCategories();
    $data['locations']=$this->ServiceModel->getTenDemography();
    $data['business']=$this->BusinessModel->getNameId();
        $this->load->view('temps/header');
        $this->load->view('temps/menu');
        $this->load->view('pages/home',$data);
        $this->load->view('temps/footer');
}

public function businessView($bid){
    $data['business'] = $this->BusinessModel->getById($bid);
    $data['services'] = $this->ServiceModel->getBusiness($bid);
      $this->load->view('temps/header');
      $this->load->view('temps/menu');
      $this->load->view('pages/businessview',$data);    

}

public function serviceView($sid){
  $data['services'] = $this->ServiceModel->getById($sid);
    $this->load->view('temps/header');
    $this->load->view('temps/menu');
    $this->load->view('pages/serviceview',$data); 
}
    
public function login_view(){
  if($this->session->user_id != null) {
        redirect('user','refresh');
        }else{
            $this->load->view("temps/header");
            $this->load->view('temps/menu');
            $this->load->view("pages/login1.php");
        }
}
public function passresetview(){
  if($this->session->user_id != null) {
        redirect('user','refresh');
        }else{
            $this->load->view("temps/header");
            $this->load->view('temps/menu');
            $this->load->view("pages/passreset.php");
        }
}

public function forgotpass_view(){
  if($this->session->user_id != null) {
        redirect('user','refresh');
        }else{
            $this->load->view("temps/header");
            $this->load->view('temps/menu');
            $this->load->view("pages/forgotpassview.php");
        }
}

  
public function register_view(){
  if($this->session->user_id != null) {
      redirect('user','refresh');
        }else{
            $this->load->view("temps/header");
            $this->load->view('temps/menu');
            $this->load->view("pages/register2.php");
        }
}
   
public function comingsoon(){
        $this->load->view("temps/header");
        $this->load->view('temps/menu');
        $this->load->view('pages/coming');
    }

}
