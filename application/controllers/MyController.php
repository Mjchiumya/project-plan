<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/EmailController.php");

class MyController extends CI_Controller {


  public function __construct(){
		parent::__construct();
      $this->load->model(array('Mymodel','EventModel','UserModel','BusinessModel'));
      $this->load->library(array('cart','session','form_validation'));
      $this->load->helper(array('form','url'));
  }


 public function cancelOrder($eid){
     if($this->Mymodel->orderCancel($eid)){
	       $this->session->set_flashdata('order_msg', 'order cancelled!');
	 	 redirect('user');
	 }else{
	 	 redirect('user');
	 }
 }

public function resetPassword(){
   $this->form_validation->set_rules('password','password','required|trim|max_length[30]|min_length[6]');
   $this->form_validation->set_rules('passconfirm','passconfirm','required|trim|matches[password]'); 
   if ($this->form_validation->run()==false) {
   	   $this->load->view("temps/header");
            $this->load->view('temps/menu');
            $this->load->view("pages/passreset.php");
   }else{
   	     $pass = $this->input->post('password');
   	     $id = $this->input->post('id');
   	     $this->UserModel->updatePassword($id,$pass);
   	     redirect('login');
   }
}

 
}







