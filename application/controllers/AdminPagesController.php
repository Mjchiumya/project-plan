<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * pagesController short summary.
 *
 * pagesController description.
 *
 * @version 1.0
 * @author acer
 */
class AdminPagesController extends CI_Controller{

    //constructor
    public function __construct(){
		  parent::__construct();
        $this->load->model(array('Mymodel','EventModel','AdminModel','BusinessModel','ServiceModel','UserModel'));
        $this->load->library(array('cart','session','form_validation'));
        $this->load->helper(array('form','url'));
       }

    public function adminIndex(){       
      $data['ucount'] = $this->AdminModel->usersCount();
      $data['scount'] = $this->AdminModel->serviceCount();
      $data['bcount'] = $this->AdminModel->businessCount();              
       $this->load->view('admin_pages/header');
       $this->load->view('admin_pages/admin/admin',$data);
       $this->load->view('admin_pages/footer');       
     }

   public function userview(){
       
       $this->load->view('admin_pages/header');
       $this->load->view('admin_pages/admin/userview');
        
     }  

   public function addBusiness(){
       $this->load->view('admin_pages/header');
       $this->load->view('admin_pages/admin/businessadd');
        $this->load->view('admin_pages/footer');
     }

  public function addService(){
       $this->load->view('admin_pages/header');
       $this->load->view('admin_pages/admin/serviceadd');
        $this->load->view('admin_pages/footer');
     }    
     
	public function service(){
       $this->load->view('admin_pages/header');
       $this->load->view('admin_pages/admin/service');
        $this->load->view('admin_pages/footer');
     }
public function adminOdersView(){
       $this->load->view('admin_pages/header');
       $this->load->view('admin_pages/admin/ordersview');
        $this->load->view('admin_pages/footer');
     }
  public function addLogo(){
       $this->load->view('admin_pages/header');
       $this->load->view('admin_pages/admin/logoform',array('error' => ' ' ));
        $this->load->view('admin_pages/footer');
     }

      public function uploadProductImage(){
       $this->load->view('admin_pages/header');
       $this->load->view('admin_pages/admin/productimgform',array('error' => ' ' ));
        $this->load->view('admin_pages/footer');
     }

    public function businessEditor($bid){
       $data['business'] = $this->BusinessModel->getById($bid);
       $data['service']  = $this->ServiceModel->getByBid($bid);          
       $data['active']  = $this->ServiceModel->getActive($bid);
       $data['inactive']  = $this->ServiceModel->getInactive(intval($bid));       
         $this->load->view('admin_pages/header');
         $this->load->view('admin_pages/admin/businessedit',$data);
         $this->load->view('admin_pages/footer');
     } 

     public function serviceEditor($sid){       
       $data['services']  = $this->ServiceModel->getById($sid);
       $data['bid'] = $this->ServiceModel->getByBId($sid);             
         $this->load->view('admin_pages/header');
         $this->load->view('admin_pages/admin/serviceedit',$data);
         $this->load->view('admin_pages/footer');
     } 
}
