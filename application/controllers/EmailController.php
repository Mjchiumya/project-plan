<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class EmailController extends CI_Controller {

    public function __construct() {
        parent:: __construct(); 
        parent::__construct();
      $this->load->model(array('Mymodel','EventModel','UserModel','BusinessModel','AuthModel'));
      $this->load->library(array('cart','session','form_validation'));
      $this->load->helper(array('form','url'));
  }
  
public function recover(){
  $this->load->config('email');
   $this->load->library('email');
    $email = $this->input->post('email');
     $uid=null;

      if(!$this->AuthModel->email_check($email)){
        $id= $this->UserModel->getId($email);         
                        
            $from = $this->config->item('smtp_source');
            $to = $email;
            $subject ='Password Recovery';
            $message = "your password Recovery request was successfull click the link or copy it in a new tab \n ".base_url()."reset/".$id;

          $this->email->set_newline("\r\n");
          $this->email->from($from);
          $this->email->to($to);
          $this->email->subject($subject);
          $this->email->message($message);

           if ($this->email->send()) {
             $this->session->set_flashdata('success_msg','email sent to your registered email account!');
             $this->load->view("temps/header");
              $this->load->view("temps/menu");
               $this->load->view("pages/forgotpassview.php");          
           } else {
               show_error($this->email->print_debugger());
             }

        }else{
             $this->session->set_flashdata('error_msg','email not found!'); 
             $this->load->view("temps/header");
            $this->load->view("temps/menu");
            $this->load->view("pages/forgotpassview.php"); 
          } 
  }  

public function sendOrder($eid){
     if($this->Mymodel->serviceOrder($eid)){
          $uid = $this->session->user_id;
          
          $data['event']=$this->EventModel->read_event_with_id($uid,$eid);
          $data['user'] = $this->UserModel->get_emailDetails($uid);
          $data['eventprofile'] = $this->EventModel->get_emailBody($eid);
          $data['bid'] = $this->EventModel->getBid($eid); 
          $this->EventModel->ordering($eid,$uid);

          $bids = array_column($data['bid'], 'bid');
          $data['emails']=$this->BusinessModel->getEmail($bids);
          $emails= array_column($data['emails'], 'b_email');
          
          $this->load->config('email');
          $this->load->library('email');

        $from = $this->config->item('smtp_source');
        $to = $emails;
        $subject ='New order from perfect-plan';
        $message = $this->load->view('pages/emailview',$data,true);

        $this->email->set_newline("\r\n");
        $this->email->from($from);
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);
        if ($this->email->send()) {
           $this->session->set_flashdata('order_msg', 'order sent!');
           redirect('user');
           } else {
            show_error($this->email->print_debugger());
             }         
      }else{
         $this->session->set_flashdata('order_msg', 'empty order! please add services to your event.');
     redirect('user');
   }
 }
 
}