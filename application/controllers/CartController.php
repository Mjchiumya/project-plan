<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * cartController short summary.
 *
 * cartController descCartController @version 1.0
 * @author acer
 */
class CartController extends CI_Controller{
    //constructor
    public function __construct(){
		parent::__construct();
        $this->load->model(array('Mymodel','EventModel','ServiceModel'));
        $this->load->library(array('cart','session','form_validation'));
        $this->load->helper(array('form','url'));
    }

    //create cart

    public function create_cart($eid)
    {
        if ($this->session->user_id != null) {
            $data['evtprof'] = $this->Mymodel->load_db_cart($eid);
            if ($data['evtprof'] != false) {
                foreach ($data['evtprof'] as $value) {
                    $data = array(
                        'id' => $value['sid'],
                        'bid'=>$value['bid'],
                        'name' => $value['s_name'],
                        'qty' => $value['qty'],
                        'price' => $value['s_price']
                    );

                    $this->cart->insert($data);
                    $this->EventModel->del_evtprofile($eid);
                    unset($data['evtprof']);
                }
            }

            $this->load->library('pagination');
            //pagination config
            $config = array();
            $config['base_url'] = base_url() . '/addservice/' . $eid;
            $config['uri_segment'] = 3;
            $config['total_rows'] = $this->ServiceModel->counts();
            $config['per_page'] = 9;
            //styling
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['next_link'] = 'Next';
            $config['prev_link'] = 'Prev';
            $config['next_tag_open'] = '<li class="pg-next">';
            $config['next_tag_close'] = '</li>';
            $config['prev_tag_open'] = '<li class="pg-prev">';
            $config['prev_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
            //initialize pagination library
            $this->pagination->initialize($config);
            $page = $this->uri->segment(3);
            $data['page'] = $page;
            $data['eid'] = array('eid' => $eid);
            $data['location'] = $this->ServiceModel->allCategories();
            $data['category'] = $this->ServiceModel->allDemography();

            //filter args
            $category = $this->input->post('category');
            $location = $this->input->post('location');

                if (!isset($category) && !isset($location)) {
                    $data["services"] = $this->ServiceModel->all($config["per_page"], $page);

                } elseif (isset($category) && !isset($location)) {
                    $data["services"] = $this->Mymodel->get_all_category_filtered($config["per_page"], $page, $category);

                } elseif (!isset($category) && isset($location)) {
                    $data["services"] = $this->Mymodel->get_all_location_filtered($config["per_page"], $page, $location);

                }

                $data["services"] = $this->ServiceModel->all($config["per_page"], $page, $location, $category);

                
                $data['cade']= array($category,$location);
                $this->load->view("temps/header");
                $this->load->view('temps/menu');
                $this->load->view("pages/createevent",$data);
            }

        else{
                redirect('login', 'refresh');
            }
        }

    //add item to cart
    public function addToCart(){
        $data = array(
          'id' => $this->input->post('s_id'),
          'bid'=>$this->input->post('bid'),
          'name' => $this->input->post('s_name'),
          'qty' => $this->input->post('quantity'),
          'price' => $this->input->post('s_price')
           );
        $this->cart->insert($data);
        echo $this->view();
    }

    //load cart
    public function loadCart(){
        echo $this->view();
    }
    //remove item in cart
    public function removeItem() {
        $id = $this->input->post("row_id");
        $data = array(
         'rowid'  => $id,
         'qty'  => 0
        );
        $this->cart->update($data);
        echo $this->view();
    }

    //clear cart
    public function clearCart(){
        $this->cart->destroy();
        echo $this->view();
    }

    //view cart contents
    public function view(){
        $output = '';
        $output .= '
  <h3>Service Builder</h3><br />
  <div class="table-responsive">
   <div align="right">
    <button type="button" id="clear_cart" class="btn btn-large red">Clear all</button>
   </div>
   <br />
   <table class="table table-bordered">
    <tr>
     <th width="18%">Name</th>
     <th width="18%">Qty</th>
     <th width="18%">Price</th>
     <th width="18%">Total</th>
     <th width="18%">Action</th>
    </tr>';

        $count = 0;
        foreach($this->cart->contents() as $items)
        {
            $count++;
            $output .= '<tr>

          <td>'.$items["name"].'</td>
          <td>'.$items["qty"].'</td>
           <td>'.$items["price"].'</td>
           <td>'.$items["subtotal"].'</td>
           <td><a name="remove" class="red btn btn-floatin remove_inventory" id="'.$items["rowid"].'"><i class="material-icons">delete</i></a></td>
       </tr>';}
        $output .= '
   <tr>
    <td colspan="4" align="right">Total</td>
    <td>'.$this->cart->total().'</td>
   </tr>
  </table>
  <br>
  </div>';

        if($count == 0) {
            $output = '<h3 align="center">Empty..</h3>';
        }
        return $output;
    }

    //insert cart contents to database
    public function cartdb($eid){
        
        $cartthings = $this->cart->contents();        
        $uid = $this->session->user_id;
        $currentEventId =intval($eid);
        if(isset($currentEventId)){
            if (isset($cartthings)) {

                 $data = array();
                foreach ($cartthings as $key) {
                     $data = array(
                      'eid'=> $currentEventId,                 
                       'sid' =>$key['id'],
                       'sname' =>$key['name'],
                       'bid' =>$key['bid'],                 
                       'qty'=>$key['qty'],
                       'total'=>$key['subtotal']                   
                     );
                     
                  $this->Mymodel->cartdbmode($data);
                }
                $this->cart->destroy();
                redirect('user','refresh');
            }else{
                $this->cart->destroy();
                redirect('user','refresh');
            }
        }
    }
}
