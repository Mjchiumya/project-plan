<?php
/**
 * Created by PhpStorm.
 * User: acer
 * Date: 17/01/2019
 * Time: 11:02
 */

class AdminController extends CI_Controller {


 public function __construct(){
    parent::__construct();
     $this->load->model(array('AuthModel','Mymodel','AdminModel','ServiceModel','BusinessModel','UserModel','EventModel'));
      $this->load->library(array('cart','session','form_validation'));
       $this->load->helper(array('form','url'));        
}      

public function addBusiness(){  

    $this->form_validation->set_rules('name','name','required|trim|max_length[20]');
     $this->form_validation->set_rules('location','location','required|trim');
      $this->form_validation->set_rules('bio','bio','required|trim|max_length[200]');
       $this->form_validation->set_rules('email','email','required|trim|valid_email');
        $this->form_validation->set_rules('phone','phone','required|numeric');
         $this->form_validation->set_rules('openTime','openTime','required|trim');    

            if($this->form_validation->run() == false){                
                 $this->load->view('admin_pages/header');
                  $this->load->view('admin_pages/admin/businessadd');
                   $this->load->view('admin_pages/footer');
               } 
               else { 

                $business = array(
                 'b_name'=>$this->input->post('name'), 
                 'b_img'=>'null',             
                 'b_location'=>$this->input->post('location'),
                 'b_phone'=>$this->input->post('phone'),
                 'b_email'=>$this->input->post('email'),                                     
                  'b_bio'=>$this->input->post('bio'),
                  'b_openhrs'=>$this->input->post('openTime')
                  );

                $this->AdminModel->insertToBusiness($business); 
                 redirect('admin-business-view','refresh');             
                 }      
			
}

public function addService(){           
        $this->form_validation->set_rules('name','name','required|trim|max_length[20]');
        $this->form_validation->set_rules('category','category','required|trim');
        $this->form_validation->set_rules('demography','demography','required|trim');
        $this->form_validation->set_rules('price','price','required|numeric');
        $this->form_validation->set_rules('bio','bio','required|trim|min_length[20]');     
        if($this->form_validation->run() == false){                
                $this->load->view('admin_pages/header');
                  $this->load->view('admin_pages/admin/serviceadd');
                   $this->load->view('admin_pages/footer');                
                } 

               else { 

                $service = array(
                 's_name'=>$this->input->post('name'),                            
                 's_category'=>$this->input->post('category'),
                 's_demography'=>$this->input->post('demography'),
                 's_price'=>$this->input->post('price'),                                     
                  's_info'=>$this->input->post('bio'),
                  'b_id'=>intval($this->input->post('id'))               
                  );

                $this->AdminModel->insertToService($service); 
                 redirect('admin-service-view','refresh');             
                 }      
      
}


public function businessEditorForm(){
 $editData = array(
                 'b_name'=>$this->input->post('name'),                             
                 'b_location'=>$this->input->post('location'),
                 'b_phone'=>$this->input->post('phone'),
                 'b_email'=>$this->input->post('email'),                                     
                  'b_bio'=>$this->input->post('bio'),
                  'b_openhrs'=>$this->input->post('hours')
                );

  $bid = intval($this->input->post('bid'));
  $this->BusinessModel->update($bid,$editData);

  redirect('admin');
}

public function serviceEditorForm(){
 $editData = array(
                 's_name'=>$this->input->post('name'),                             
                 's_category'=>$this->input->post('category'),
                 's_demography'=>$this->input->post('demography'),
                 's_price'=>$this->input->post('price'),                                     
                  's_info'=>$this->input->post('info')                
                );

  $bid = intval($this->input->post('bid'));
  $this->ServiceModel->update($bid,$editData);
  redirect('admin');
}

public function adminServices(){
          $udata = $this->Mymodel->all_services();
          echo(json_encode($udata));
}

public function adminOrdersJson(){
          $data['draw'] =1;
          $data['recordsTotal']=$this->EventModel->ordercount();
          $data['recordsFilter']=10;
          $data = $this->EventModel->getOrderDetails();
          echo(json_encode($data));
}

public function adminUsers(){
  $data['draw'] =1;
  $data['recordsTotal']=$this->UserModel->counts();
  $data['recordsFilter']=10;
  $data['data']= $this->UserModel->getAll();
   echo(json_encode($data));
}

public function adminBusjson(){
          $udata = $this->Mymodel->all_businesses();
          echo(json_encode($udata));
}
public function serviceCategories(){
          $udata = $this->AdminModel->servicePieData();
          echo(json_encode($udata));
}

public function activateServiceStatus(){
    $sid = intval($this->uri->segment(2));
    $this->ServiceModel->activateStatus($sid);
    redirect('admin');
}

public function deactivateServiceStatus(){
    $sid = intval($this->uri->segment(2));
    $this->ServiceModel->deactivateStatus($sid);
    redirect('admin');
}

public function deleteService(){
    $sid = intval($this->uri->segment(2));
    $this->ServiceModel->destroy($sid);
    redirect('admin');
}


public function logoUpload(){
      $bid = intval($this->input->post('id'));
      $config['upload_path']   = './uploads';
      $config['allowed_types'] = 'gif|jpg|png';
      $config['max_size']      = 400;
      $config['max_width']     = 1024;
      $config['max_height']    = 768;
      $this->load->library('upload', $config);
                if ( ! $this->upload->do_upload('userfile')){
                        $error = array('error' => $this->upload->display_errors());
                       // redirect($_SERVER['HTTP_REFERER']);
                        $this->load->view('admin_pages/header');
                        $this->load->view('admin_pages/admin/logoform',$error);
                        $this->load->view('admin_pages/footer');
                }else
                {
                        
                        $upload_data = $this->upload->data(); 
                        $file_name = trim($upload_data['file_name'],'\t');                     
                        $this->BusinessModel->uploadLogo($file_name,$bid);
                        redirect('admin-bedit/'.$bid);
                }
}

public function imageUpload(){      
      $config['upload_path']   = './uploads';
      $config['allowed_types'] = 'gif|jpg|png';
      $config['max_size']      = 400;
      $config['max_width']     = 1024;
      $config['max_height']    = 768;
      $this->load->library('upload', $config);
                if ( ! $this->upload->do_upload('userfile')){
                        $error = array('error' => $this->upload->display_errors());
                       // redirect($_SERVER['HTTP_REFERER']);
                        $this->load->view('admin_pages/header');
                        $this->load->view('admin_pages/admin/productimgform',$error);
                        $this->load->view('admin_pages/footer');
                }else
                {     
                       $modelData = array();
                      $image = intval($this->uri->segment(2));
                      $upload_data = $this->upload->data(); 
                      $file_name = $upload_data['file_name'];

                       switch ($image) {
                       case 1:
                        $modelData['img1'] = $file_name;
                        break;
                       case 2:
                        $modelData['img2']  = $file_name; 
                       break;
                       case 3:
                       $modelData['img3'] = $file_name;
                       break;    
        
                       default:
                       # code...
                       break;
                  }
                        
                        $id = intval($this->uri->segment(3));                        
                        $this->ServiceModel->upDateImage($modelData,$id);
                        redirect('admin-sedit/'.$id);
                }
} 
 
public function deleteBusiness(){
  $bid = intval($this->uri->segment(2));  
    $this->BusinessModel->destroy($bid);
    redirect('admin-business-view');
}

public function deleteAllServices(){
  $bid = intval($this->uri->segment(2));  
    $this->ServiceModel->destroyAll($bid);
    redirect('admin-service-view');
}  


}
