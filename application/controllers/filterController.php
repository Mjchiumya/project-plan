<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class filterController extends CI_Controller {

public function __construct(){ 
         parent::__construct();
         $this->load->helper('url');       
         $this->load->library('session');        
         $this->load->model(array('filterModel','ServiceModel'));
         }

public function index(){          
     $search = $this->input->post('search'); 
     $location = $this->input->post('location');
     $this->load->library('pagination');      
   
      $config = array();
            $config['base_url'] = base_url().'/admin-business-view/';
            $config['uri_segment'] = 2;
            $config['total_rows'] = $this->filterModel->countBusiness();
            $config['per_page'] = 6;            
            $config['num_tag_open'] = '<li class="pagination-item">';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['next_link'] = 'Next';
            $config['prev_link'] = 'Prev';
            $config['next_tag_open'] = '<li class="pg-next">';
            $config['next_tag_close'] = '</li>';
            $config['prev_tag_open'] = '<li class="pg-prev">';
            $config['prev_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li class="pagination-item">';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="pagination-item">';
            $config['last_tag_close'] = '</li>';
            //initialize pagination library
            $this->pagination->initialize($config); 
             $page = $this->uri->segment(2);
            
$data['business'] = $this->filterModel->getAllBusiness($config['per_page'],$page,$search);     	      
            $this->load->view("admin_pages/header");  
            $this->load->view("admin_pages/admin/filter",$data); 
            $this->load->view("admin_pages/footer");  
                           
        }

public function serviceFilter(){ 

    $search = $this->input->post('search'); 
    $location = $this->input->post('location'); 
    $this->load->library('pagination');  
     
    //pagination config
    $config = array();

            $config['base_url'] = base_url().'/admin-service-view/';
            $config['uri_segment'] = 2;
            $config['total_rows'] = $this->ServiceModel->counts();
            $config['per_page'] = 6;
            //styling
            $config['num_tag_open'] = '<li class="pagination-item">';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['next_link'] = 'Next';
            $config['prev_link'] = 'Prev';
            $config['next_tag_open'] = '<li class="pg-next">';
            $config['next_tag_close'] = '</li>';
            $config['prev_tag_open'] = '<li class="pg-prev">';
            $config['prev_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li class="pagination-item">';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li class="pagination-item">';
            $config['last_tag_close'] = '</li>';
            //initialize pagination library
            $this->pagination->initialize($config); 
             $page = $this->uri->segment(2);
            
           $data['service'] = $this->filterModel->getAllService($config['per_page'],$page,$search);            
            $this->load->view("admin_pages/header");  
            $this->load->view("admin_pages/admin/servicefilter",$data); 
            $this->load->view("admin_pages/footer");  
                           
        }


  

        }
           
  

   
  