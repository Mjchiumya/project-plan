<!DOCTYPE html>
  <html lang="en">
    <head>      
       <title>Perfect Plan</title>      
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>      
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">  
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">
      <link rel="stylesheet" href="<?php echo base_url();?>/css/main.css">
       <link rel="stylesheet" href="<?php echo base_url();?>/css/admin.css">
        <link rel="stylesheet" href="<?php echo base_url();?>/css/footer.css">
      <link rel="stylesheet" href="<?php echo base_url();?>/css/curtainMenu.css">
      <link href="https://fonts.googleapis.com/css?family=Ubuntu+Condensed" rel="stylesheet">
      <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css"> 
    </head>


<body>
  <div class="navbar-fixed">
    <nav>
    <div class="nav-wrapper">
      <a href="<?php echo base_url();?>">
        <img class="bland-logo" width="120" height="70" src="<?php echo base_url();?>img/sys/logo.png">
      </a>

      <ul class="right">       
        <?php
        if ($this->session->userdata('user_name')) {
          echo '
           <li class="dropdown">
             <a class="dropbtn truncate" href="javascript:void(0)">
               <i class="material-icons left">person</i>'.
                $this->session->userdata("user_name").'
              </a>
             <div class="dropdown-staff">
              <a href="'. base_url("user").'">Account</a>             
              <a href="#">Edit Account</a>
              <a href="'.base_url("eventprofile").'">create Event</a>                
              <a class="truncate" href="'.base_url("logout").'">Logout</a>             
            </div>
           </li>'
           ;}?>
        <li><a href="javascript:void(0)" onclick="openNav()"><i class="material-icons">list</i></a></li>
      </ul>
      <!-- Dropdown Structure -->
               
    </div>
  </nav>     
  </div> 

