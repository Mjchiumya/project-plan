
<div class="col s12">
  <nav>
  <div class="nav-wrapper">
     <form method="post" action="">
       <div class="input-field">
          <input id="search" type="search" name="search" required>
          <label class="label-icon" for="search"><i class="material-icons">search</i></label>
          <i class="material-icons">close</i>
        </div>
      </form>
   </div>
  </nav>
<br>
<p class="grey lighten-2 btn-flat modal-trigger" href="#modal1">
   <i class="material-icons left">add_shopping_cart</i>     
 </p>
 <p class="grey lighten-2 btn-flat modal-trigger" href="#modal2"><i class="material-icons left">filter_list</i>
 </p>
 <p class="grey lighten-2 btn-flat modal-trigger"><i class="material-icons left">search</i>
 </p>

 <div class="row">
  <div class="col m6"> 
  <div class="row">
     <div class="col s6">         
                  <!-- Dropdown Structure -->
          <ul id='dropdown1' class='dropdown-content'>
           <li><a href="<?php echo base_url()?>admin-service-view">view all</a></li>
           <li><a href="#!">two</a></li>
           <li class="divider" tabindex="-1"></li>
           <li><a href="#!">three</a></li>
           <li><a href="#!"><i class="material-icons">view_module</i>four</a></li>
           <li><a href="#!"><i class="material-icons">cloud</i>five</a></li>
          </ul>       
     </div>
       
  </div>        
  
   </div>     
  </div>
</div>
        

</div>
 

<div class="section ">
  <!-- Modal Structure -->
  <div id="modal1" class="modal modal-fixed-footer">
    <div class="modal-content">
      <div id="cart_details" class="white">
        <h3 align="center">Event is Empty</h3>
       </div>
    </div>
    <div class="modal-footer">      
      <a href=" <?php echo base_url()."eventreg/".$eid['eid'];?>" class="modal-close waves-effect waves-green btn-flat">Save</a>
      <a href="#!" class="modal-close waves-effect waves-green btn-flat">close</a>
    </div>
  </div>



   <!-- Modal filter -->
  <div id="modal2" class="modal modal-fixed-footer">
    <div class="modal-content">
      <form id="cart_details" class="white" method="post" action="<?php echo base_url().'addservice/'.$eid['eid']?>">

          <div class="input-field col s12 m8">
              <input type="text" name="searchproduct" placeholder="search ">
          </div>

         <div class="input-field col s12 m8">
           filter by location
             <select class="browser-default" name="location">
                 <option>choose location</option>
                 <?php foreach ($location as $loc){
                 echo '<option value="'.$loc->s_demography.'" >'.$loc->s_demography.'</option>';
                 }?>
             </select>
         </div>

          <div class="input-field col s12 m8">
              filter by category
              <select class="browser-default"  name="category">
                  <option>choose category</option>
                  <?php foreach ($category as $cat){
                      echo '<option value="'.$cat->s_category.'">'.$cat->s_category.'</option>';
                  }?>
              </select>
          </div>

          <div class="input-field col s12">
              order by price
              <!-- Switch -->
              <div class="switch">
                  <label>
                      low price
                      <input type="checkbox">
                      <span class="lever"></span>
                      high price
                  </label>
              </div>
          </div>

          <br>
          <input class="red btn-flat white-text" type="submit"/>
      </form>
    </div>
    <div class="modal-footer">

      <a href="#!" class="modal-close waves-effect waves-green btn-flat">close</a>
    </div>
  </div>
  </div>

<div class="row">
<?php
  if(!empty($services)) {
      foreach ($services as $service) {
          echo '

  <div class="col s12 m4">
   <div class="card small ">    
    <div class="card-image waves-effect waves-block waves-light" >
     <img class="activator" 
     src="' . base_url() . 'uploads/' . $service->img2 . '" width="100%">
        <span class="card-title grey truncate">'
        . ucfirst($service->s_category) . '</span>
   </div >

<div class="card-content" >
   <span class="card-title activator grey-text text-darken-4" > ' . $service->s_name . '<i class="material-icons right" > more_vert</i > </span >
</div>

<div class="card-reveal" >
<span class="card-title grey-text text-darken-4" > ' . ucfirst($service->s_name) . '<i class="material-icons right">close</i > </span >

<a class="btn disabled">' . $service->s_demography . '</a>
<a class="btn disabled">K' . $service->s_price . '</a><br>
<span class="grey-text">' . $service->s_info . '</span>
<div class="input-field">
<input type="text" name="quantity" class="form-control quantity" id="' . $service->s_id . '" / > <br >
<a name="add_cart" class="btn grey white-text add_cart" data-productname="' .$service->s_name.'" data-price="'.$service->s_price.'" data-productid="'.$service->s_id.'" data-bid="'.$service->b_id.'"> Add</a>
</div>
</div>
</div>
</div>
 ';
      }
  }
  ?>
  <br>
</section>

  <ul class="pagination center-align">
    <?php echo $this->pagination->create_links(); ?>
</ul>
</div>
 
      <!-- Compiled and minified JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>/jquery/jqueryc.js"></script>  
<script type="text/javascript" src="<?php echo base_url();?>/jquery/menu.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>jquery/steps/step.custom.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>jquery/scroll.js"></script>

    </body>
  </html>

<script type="text/javascript">
$(document).ready(function(){

    //$('select').formSelect();

 $('#publish').click(function(){
  
 });

 $('.add_cart').click(function(){
  var product_id = $(this).data("productid");
  var b_id = $(this).data("bid");
  var product_name = $(this).data("productname");
  var product_price = $(this).data("price");
  var quantity = $('#' + product_id).val();
  if(quantity != '' && quantity > 0)
  {
   $.ajax({
    url:"<?php echo base_url(); ?>/cartadd",
    method:"POST",
    data:{s_id:product_id, bid:b_id, s_name:product_name, s_price:product_price, quantity:quantity},
    success:function(data)
    {
     alert("Product Added into Cart");
     $('#cart_details').html(data);
     $('#' + product_id).val('');
    }
   });
  }
  else
  {
   alert("Please Enter quantity");
  }
 });

 $('#cart_details').load("<?php echo base_url();?>/cartload");

 $(document).on('click', '.remove_inventory', function(){
  var row_id = $(this).attr("id");
  if(confirm("Are you sure you want to remove this?"))
  {
   $.ajax({
    url:"<?php echo base_url(); ?>cartitemdel",
    method:"POST",
    data:{row_id:row_id},
    success:function(data)
    {
     alert("Product removed from Cart");
     $('#cart_details').html(data);
    }
   });
  }
  else
  {
   return false;
  }
 });

 $(document).on('click', '#clear_cart', function(){
  if(confirm("Are you sure you want to clear cart?"))
  {
   $.ajax({
    url:"<?php echo base_url(); ?>/clear",
    success:function(data)
    {
     alert("Your cart has been clear...");
     $('#cart_details').html(data);
    }
   });
  }
  else
  {
   return false;
  }
 });

});
</script>
</body>
</html>
