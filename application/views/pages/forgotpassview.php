<div class="section"></div>
<div class="section"></div>
  
<center>     
<div class="container">
 <div class="z-depth-1 grey lighten-4 row" style="display: inline-block; padding: 4rem; border: 1px solid #EEE;">

<?php echo form_open("emailme");?>           
<h5 class="prim">Forgot Password</h5>
<?php
      $success_msg = $this->session->flashdata('success_msg');   
      $error_msg = $this->session->flashdata('error_msg');          
         if($error_msg){
           ?>
          <div class="red-text">
           <?php echo $error_msg; ?>
              </div>
                <?php
                  }?>
         <?php if($success_msg){
           ?>
          <div class="alert">
           <?php echo $success_msg; ?>
              </div>
                <?php
                  }?> 

      <br>
<div class='row'>
  <div class='input-field col s12'>
    <input class='validate'type='email' autocomplete="email" name='email' id='email' required />
    <label class="active" for='email'>Enter your email</label>
  </div>
</div>

            
<br />
            <center>
              <div class='row'>
                <button type='submit' name='btn_login' class='col s12 btn btn-large waves-effect red'>Send</button>
                
              </div>
            </center>
          </form>
        </div>
      </div>      
    </center>

    <div class="section"></div>
    <div class="section"></div>
<!-- Compiled and minified JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>/jquery/jqueryc.js"></script>  
<script type="text/javascript" src="<?php echo base_url();?>/jquery/menu.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>jquery/steps/step.custom.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>jquery/scroll.js"></script>
  
  </body>
</html>
