<div class="container">
  <br>
   <div class="progress grey lighten-1 hide-on-small-only">
      <div class="determinate red" id="bar"></div>      
  </div>
  
 
 <br>
 <div class="red white-text">
   <?php echo validation_errors(); ?>
 </div>

<?php echo form_open('form');?>
  <div id="account_details" class="qontainer">
     <h6 class="sub-title">1 of 3</h6>
    <h3 class='form_head'> Event Organiser</h3>   
      <div class="row">
        <div class="input-field col s6">         
        <input  class="validate" name="organiser" type="text" />
         <label class="active" for="organiser">Organiser</label>
        </div>
        <div class="input-field col s6">
          <input class="validate" type="text" name="name"/ >
          <label for="name">Event name</label>
         </div>
      </div>
            
    <br>
    <input type="button" class="blue-grey lighten-3" value="Next" onclick="show_next('account_details','contact_details','bar');">   
  </div>


  <div id="contact_details" class="qontainer">
       <h6 class="sub-title">2 of 3</h6>
    <h3 class='form_head'>Event Details</h3>
  <div class="row">        
      <div class="input-field col s6">
       <input type="date" name="evtdate" class="datepicker">
       <label for="evtlocation">Event Date</label>    
      </div>
      <div class="input-field col s6">
        <input name="evttime" type="time" class="timepicker"> 
        <label for="evtlocation">Event Time</label>    
     </div>

  </div>
  <div class="row">
    <div class="input-field col s6">
     <input class="validate" name="evtlocation" type="text"/ >
      <label for="evtlocation">Event Location</label>      
    </div>
  </div>      
    <input type="button" class="blue-grey lighten-3" value="Previous" onclick="show_prev('account_details','bar');">
    <input type="button" class="blue-grey lighten-3" value="Next" onclick="show_next('contact_details','extra_details','bar');">
  </div>
        
  <div id="extra_details" class="qontainer">
       <h6 class="sub-title">3 of 3</h6>
    <h3 class='form_head'>Extra Details</h3>
     <div class="row">
        <div class="input-field col s6">
          <select name="evttype" class="browser-default">  
           <option value="" disabled selected>Choose your option</option>       
          <option value="private">Private</option>
           <option value="public">Public</option>      
        </select>
               
        </div>
        <div class="input-field col s6">
          <input name="evtcharge" type="number"/ >
           <label for="evtcharge">Event Charge</label>  
        </div>
        </div> 
       
<div class="input-field col s12">
 <textarea class="materialize-textarea" name="evtmsg" data-length="120"></textarea>
  <label for="evtmsg">information</label>
</div> 
    <input type="button" class="blue-grey lighten-3" value="Previous" onclick="show_prev('contact_details','bar');">
    <input type="Submit" class="blue-grey lighten-3" value="Submit">
  </div>
  </form>
  
</div>
     <!-- Compiled and minified JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>/jquery/jqueryc.js"></script>  
<script type="text/javascript" src="<?php echo base_url();?>/jquery/menu.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>jquery/steps/step.custom.js"></script>

    </body>
  </html>