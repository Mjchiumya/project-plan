<div class="section col s12" style="background: url('https://picsum.photos/900?image=210') no-repeat;
  background-position: center; /* Center the image */
  background-repeat: no-repeat; /* Do not repeat the image */
  background-size: cover;
  ">
  <br>
  <div class="row">

    <div class="col s12 m3">
      <div class="card-panel" style="background-color: rgb(0,0,0);
  background-color: rgba(0,0,0, 0.9);">
       
        <h1 class="header white-text"><i class="material-icons circle large grey-text">person</i><br>
          <?php echo ucfirst($this->session->userdata('user_name')); ?></h1>      
      </div>
      
<div class="card-panel">
  
  <ul>
      <li class="waves-effect waves-light"><a href="<?php echo base_url("eventprofile");?>" class="btn orange darken-5 white-text"><i class="material-icons">add</i></a></li>
      <li class="waves-effect waves-light"><a href="#!" class="btn orange darken-4 white-text"><i class="material-icons">thumb_up</i></a></li>
      <li class="waves-effect waves-light"><a href="#!" class="btn orange darken-3 white-text"><i class="material-icons">favorite</i></a></li>
      <li class="waves-effect waves-light"><a href="#!" class="btn orange darken-2 white-text"><i class="material-icons">info</i></a></li>
    </ul>
     
</div>
      

<div class="card-panel">
    <a href="#" class="grey-text text-darken-3">
      <i class="material-icons left grey-text mb-2">email</i>
       <?php foreach ($user as $owner) {
          echo $owner->u_email;
       }('user_email');?>
      </a>
      <br><hr><br>

        <a href="#" class="grey-text text-darken-3"><i class="material-icons left grey-text">phone</i><?php foreach ($user as $owner) {
          echo $owner->u_mobile;
       }('user_email');?>
      </a>
      <br><hr><br>
        <a href="#" class="grey-text text-darken-3"><i class="material-icons left grey-text">people</i>member since<br>
          <?php foreach ($user as $owner) {
          echo date("r",strtotime($owner->created));
       }('user_email');?>
        </a>        
      </div>
      

      <div class="card-panel">
        <a href="<?php echo base_url('logout');?>" class="btn grey white-text">Logout</a> 
         <a onclick="return confirm('are you sure u want to deactivate account?')" href="<?php echo base_url('deactivate').'/'.$this->session->userdata('user_id');?>" class="right-align btn grey white-text">deactivate account</a>        
      </div>
    </div>    
       
  


<div class="container col s12 m8" style="background-color: rgb(0,0,0);
  background-color: rgba(0,0,0, 0.8);">
  <div class="row">     
        
    <div class="evtcard">
     <h1 class="subheader white-text">Notification </h1>
       <?php     
           $msg = $this->session->flashdata('order_msg');          
         if($msg){
           ?>
          <div class="white-text flow-text">
            <span class="white-text">
             <?php echo $msg; ?>
            </span>
          </div>
                <?php
                  }?>

         <h1 class="subheader white-text">Active Events</h1>
          <ul class="list">
           <?php foreach($evt as $ev){
              
            echo '
            <li class="waves-effect">
         <a class="dropdown-button" 
           href="'.base_url().'live-event/'.$ev->e_id.'"> 
              <div class="valign-wrapper">
               <i class="material-icons left circle green-text text-lighten-2">folder</i>
                 <div class="title white-text">                  
                   '.ucfirst($ev->e_name).'<br>
                 <span class="white-text flow-text">'.$ev->e_date.'</span>
                </div>
                 <i class="material-icons ml-auto white-text">info</i>
             </div>
       </a>
         </li>';
       }?>            
    </ul>
    <hr class="list-divider">
    <h1 class="subheader white-text">New Events</h1>
    <ul class="list">
    <?php foreach($drafts as $draft){
   
echo '<li class="waves-effect">
        <a class="dropdown-button" 
           href="'.base_url().'event/'.$draft->e_id.'">        
              <div class="valign-wrapper">
               <i class="material-icons left circle yellow-text text-lighten-2">
                   insert_drive_file
                   </i>
                 <div class="title white-text">
                 '.ucfirst($draft->e_name).'<br>
                  <span class="white-text flow-text">'.$draft->e_date.'</span>
              </div>          
            <i class="material-icons ml-auto grey-text text-lighten-4">info</i>            
        </div>      
      </a>     
      </li>';

    }?>
   </ul>
   <hr class="list-divider">
   <h1 class="subheader white-text">Cancelled Events</h1>
          <ul class="list">
           <?php foreach($cancel as $exed){
              
            echo '
            <li class="waves-effect">
         <a class="dropdown-button" 
           href="#"> 
              <div class="valign-wrapper">
               <i class="material-icons left circle red-text text-lighten-2">folder</i>
                 <div class="title white-text">                  
                   '.ucfirst($exed->e_name).'<br>
                 <span class="white-text flow-text">'.$exed->e_date.'</span>
                </div>
                 <i class="material-icons ml-auto  lime-text text-lighten-5">info</i>
             </div>
       </a>
         </li>';
       }?>            
    </ul>
    <hr class="list-divider">
  </div>   
    </div>      
      
  </div>
</div>
</div>

<br><br>
   
      <!-- Compiled and minified JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>/jquery/jqueryc.js"></script>  
<script type="text/javascript" src="<?php echo base_url();?>/jquery/menu.js"></script>    
</body>
</html>
