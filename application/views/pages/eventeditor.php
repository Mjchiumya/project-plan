<div class="container grey lighten-2 p2">  
<div class="container red white-text"><?php echo validation_errors(); ?></div>
<div class="section"></div>
<div class="section"></div>

<?php echo form_open("formevtedit");?>

<h3 class="prim align-center>">EVENT EDITOR</h3>

 <?php foreach($event as $evt):?>
<div class="row">
  <input type="hidden" name="id" value="<?php echo $evt->e_id?>">
  <div class="input-field col s6">
   <i class="material-icons prefix">person</i>         
   <input  class="validate" name="organiser" type="text" value="<?php echo $evt->e_organiser?>"/>  
 </div>

 <div class="input-field col s6">
  <i class="material-icons prefix">visibility</i>
  <input class="validate" type="text" name="name" value="<?php echo $evt->e_name?>"/>  
  </div>
</div>

<div class="row">
   <div class="input-field col s6">
    <i class="material-icons prefix">event_note</i>
     <input type="text" name="evtdate" class="datepicker" value="<?php echo $evt->e_date?>" />
  </div>
  <div class="input-field col s6">
    <i class="material-icons prefix">location_on</i>
      <input class="validate" name="evtlocation" type="text" value="<?php echo $evt->e_location?>"/>          
  </div>
</div>
  
<div class="row">
 <div class="input-field col s6">
   <i class="material-icons prefix">watch_later</i>
  <input name="evttime" type="time" value="<?php echo $evt->e_time?>"/> 
</div>
<div class="input-field col s6">
  <select name="evttype" class="browser-default">
    <option selected><?php echo ucfirst($evt->e_type)?></option>
    <option value="private">Private</option>
    <option value="public">Public</option>
  </select>
  
     
</div> 
</div>

<div class="row">
 <div class="input-field col s6">
   <i class="material-icons prefix">monetization_on</i>
  <input name="evtcharge" type="text"/ value="<?php echo $evt->e_charge?>">
    
  </div>
</div>  
<div class="input-field col s12">
   <i class="material-icons prefix">description</i>
  <textarea name="evtmsg" class="materialize-textarea"><?php echo $evt->e_notes?></textarea>
   
</div>


<center>
  <input type="submit" name="submit" class="btn grey">
  <a href="<?php echo base_url('user');?>" class="red btn"><i class="material-icons right">cancel</i> cancel</a> 
</center>
    <?php endforeach;?>
</form>  
</div>
<div class="section"></div>
