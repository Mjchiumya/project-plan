<!DOCTYPE html>
  <html lang="en">
    <head>      
       <title>Perfect Plan</title>      
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>      
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">  
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">
      <link rel="stylesheet" href="<?php echo base_url();?>/css/main.css">
       <link rel="stylesheet" href="<?php echo base_url();?>/css/admin.css">
        <link rel="stylesheet" href="<?php echo base_url();?>/css/footer.css">
      <link rel="stylesheet" href="<?php echo base_url();?>/css/curtainMenu.css">
      <link href="https://fonts.googleapis.com/css?family=Ubuntu+Condensed" rel="stylesheet">
      <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"> 
   </head>

<body>
<div class="container"> 
<p class="left-align black-text flow-text">There's a new order made by user below</p>
  <ul> 
  <?php foreach ($user as $value) {
  echo '<li><b>'.$value->u_name.'</b></li>'.
        '<li>Phone :'.$value->u_mobile.'</li>
        <li>Email : '.$value->u_email.'</li>';
      }?>
  </ul> 
  <br>

 <div class="table-responsive">   
   <table class="table table-bordered">
    <tr>
     <th width="25%">Service/Product</th>
     <th width="25%">price</th>
     <th width="25%">Quantity</th>
     <th width="25%">Total</th>     
    </tr>
    
    <?php foreach($eventprofile as $ev){
      echo 
      '<tr>
        <td>'.$ev->sname.'</td>
         <td>'.$ev->sname.'</td>
         <td>'.$ev->qty.'</td>
           <td>'.$ev->total.'</td>
           </tr>'
           ;}?>
    
   </table>
 <br><br>
  <ul>
    <li>All these service are ordered for the event below</li>
    <?php foreach ($event as $key) {      
      echo 
      '<li><b>Event Name: '.$key->e_name.'</b></li>
      <li>Time :'.$key->e_time.'</li>
      <li>Date :'.$key->e_date.'</li>';
       }?>
  </ul>
   <p class="left-align black-text flow-text">please contact the event organiser to comfirm order.</p>
   </div>
 </div>

<!-- Compiled and minified JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>/jquery/jqueryc.js"></script>  
<script type="text/javascript" src="<?php echo base_url();?>/jquery/menu.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>jquery/steps/step.custom.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>jquery/scroll.js"></script>
</body>
</html>