<div class="container">

<div class="card-panel"> 
   <div class="dropdown">
      <a class=" btn btn-floating deep-orange darken-2 pulse">
        <i class="material-icons">arrow_drop_down</i>
      </a>    
          
    <?php foreach($event as $ev):
      echo '         
          <div class="dropdown-staff">
          <a href="'.base_url().'editevent/'.$ev->e_id.'">Edit</a>
          <a href="'.base_url().'addservice/'.$ev->e_id.'">Add Services</a>
          <a onclick="return confirm(\'are you sure u want to send order?\')" href="'.base_url("order").'/'.$ev->e_id.'" >send order</a>       
          <a onclick="return confirm(\'are you sure u want to delete?\')" href="'.base_url().'deleteevent/'.$ev->e_id.'">
          Delete</a>             
          </div>      
        ';?>
    <?php endforeach;?>
  </div> 
</div>

<div class="col s12 bg-image">
<div class="section"></div>
<div class="section"></div>
<div class="bg-text">
<?php foreach($event as $ev):?>  
  <h2><?php echo $ev->e_location;?></h2>
  <h1 style="font-size:50px"><?php echo $ev->e_name;?></h1>
  
  <p>Draft Event organised by <span class="white-text"><?php echo $ev->e_organiser;?></span></p>
</div>
</div>

<div class="col s12">      
  <div class="collapsible-header active">
   <i class="material-icons">info</i>
    <h4>Details</h4>
  </div><br>
       
  <div class="row">
    <div class="col s6">
      <?php echo $ev->e_location;?>
        <a href="#!" class="grey-text">
          <i class="material-icons left">location_on</i>
        </a>
    </div>
  <div class="col s6">
   <?php echo $ev->e_organiser;?>
    <a href="#!" class="grey-text">
     <i class="material-icons left">person</i>             
    </a>
  </div>
</div>

<div class="row">
 <div class="col s6">
  <?php echo $ev->e_time;?>
    <a href="#!" class="grey-text">
     <i class="material-icons left">query_builder</i>
    </a>
 </div>
 <div class="col s6">
   <?php echo $ev->e_date;?>
    <a href="#!" class="grey-text">
     <i class="material-icons left">today</i>             
    </a>
 </div>
</div>

<div class="row">
  <div class="col s6">
    <?php echo $ev->e_charge;?>
      <a href="#!" class="grey-text">
        <i class="material-icons left">monetization_on</i>
      </a>
  </div>
  <div class="col s6">
   <?php echo $ev->e_type;?>
    <a href="#!" class="grey-text">
      <i class="material-icons left">group</i>
    </a>
  </div>
</div>   
      
<?php endforeach;?> 
  </div>    
  <br>

 <div class="row white">
  <?php $no=1;foreach($products as $items){
   echo '<div class="col s6 m3">
      <div class="card small grey darken-2" style="border-radius:.8rem;">
       <div class="card-content">      
        <h3 class="center-align white-text"> <span class="white badge">'.$no++.'</span>'.$items["s_name"].'</h3>
         <p class="center-align white-text">Quantity : '.$items["qty"].'<br>
          Price :  '.$items["s_price"].'<br>
          Total :  '.$items["total"].'   
         </p>
          <br>
           <div class="center">
            <a href="'.base_url("service").'/'.$items["sid"].'" class="truncate btn white grey-text text-darken-1">view product</a>
           </div> 
       </div>
  </div>
</div>'
;}?>
</div>  
<br><br>

    <!-- Compiled and minified JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>/jquery/jqueryc.js"></script>  
<script type="text/javascript" src="<?php echo base_url();?>/jquery/menu.js"></script>

</body>
</html>
