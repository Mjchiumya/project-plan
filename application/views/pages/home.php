<div>  
  <div class="slider">
    <ul class="slides"> 
      <li>
        <img class="responsive-img" src="https://media.giphy.com/media/KbTC3fzLxxDG0HgHDU/giphy.gif">        
      </li>
      <li>
         <img class="responsive-img" src="<?php echo base_url().'css/calender.gif'; ?>">    
      </li>  
</div>

</div>


<div class="section white">
 <div class="row">

  <div class="col s6 m3">
   <div class="card">
     <div class="card-content light-green accent-2">    
      <span class="card-title">Category</span>
      <ul>
         <?php foreach($categories as $cat){
           echo '<li><a class="grey-text text-darken-2" href="#">'.ucfirst($cat->s_category).'</a></li>';
          }?>
      </ul>
    </div>
    </div>
   </div> 
   

  <div class="col s6 m3">      
    <div class="card">
     <div class="card-content green accent-2">    
      <span class="card-title">Location</span>
      <ul>
        <?php foreach($locations as $loc){
        echo '<li><a class="grey-text text-darken-2"href="#">'.ucfirst($loc->s_demography).'</a></li>';
         }?>
      </ul>
    </div>
  </div>
</div>

	 <div class="col s6 m3">      
      <div class="card">
     <div class="card-content green accent-1">    
      <span class="card-title">Products</span>
      <ul>
        <?php foreach($products as $product){
         echo '<li><a class="grey-text text-darken-2" href="'.base_url().'service/'.$product->s_id.'">&#187;'.ucfirst($product->s_name).'</a></li>';
          }?>
      </ul>
    </div>
  </div>
</div>

    <div class="col s6 m3">      
      <div class="card">
     <div class="card-content light-green accent-1">    
      <span class="card-title">Business</span>
      <ul>
          <?php foreach($business as $bus){
           echo '<li><a class="grey-text text-darken-2" href="'.base_url().'business/'.$bus->b_id.'">&#187;'
            .ucfirst($bus->b_name).'</a></li>';
           }?>
      </ul>
    </div> 
  </div>
</div>

  </div>
</div>

<div class="section"></div>
<div class="section"></div>

 <div class="row">
  <?php foreach($services as $service){
 echo '
    <div class="col s12 m4">
      <div class="card small">
       <div class="card-image waves-effect waves-block waves-light">      
         <img class="responsive-img" width="100%" src="uploads/'.$service->img2.'">          
      </div>

      <div class="card-content">      
        <span class="card-title activator grey-text text-darken-4 truncate">
          '.ucfirst($service->s_name).' <br> 
           <span class=" amber accent-3 badge white-text">'.strtoupper($service->s_category).'</span>
            <i class="material-icons right">arrow_drop_up</i>         
          <span class="card-title grey-text">K '.ucfirst($service->s_price).'</span>          
        </span>      
    </div>

    <div class="card-reveal">
      <span class="card-title grey-text text-darken-4">
        <i class="material-icons right">close</i>
         '.ucfirst($service->s_name).'
       </span>
        <p class="grey-text left-align">'.ucfirst($service->s_info).'</p>
         <a href="'.base_url().'service/'.$service->s_id.'" class="btn btn-floating grey"><i class="large material-icons">info</i></a>
    </div>

  </div>
</div>
    ';}?>
 <!-- end card-tree -->

  </div>


<div class="section"></div>
<div class="section"></div>


 <section class="prim nunu">  
  <div class="row">              
    <h2 class="center-align white-text text-darken-1">Register your Business</h2> 
     <div class="section hide-on-small-only"></div>     
	    <div class="container">
		<h3 class="white-text text-darken-1 center-align">Take your business to millions of customers and let them discover your magic today</h3>
		</div>		    
       <center>
         <a href="<?php echo base_url('coming');?>" class="center-align waves-effect waves-light btn-large btn red">Register here</a>
       </center>
  </div>
 </section>
<div class="section"></div>
<!--events view-->
<div class="section">
  <div class="row">

  <div class="col s12 m4">
    <div class="eventContainerFront">
      <ul class="collection">
      <li class="collection-item avatar">
        <i class="material-icons circle start-color">
        <i class="fa fa-bell-o"></i>
        </i>
        <div class="list-info start">
        <span class="title">Kilpatrick, Brian</span>
         <p class="grey-text left-align">Starting @</p>
        <p class="grey-text left-align">The Judge Group</p>
        </div>
        <div class="secondary-content">
        <p class="grey-text right-align">8/27/16</p>
        <i class="fa fa-chevron-right right"></i>
        </div>
      </li>
  
      <li class="collection-item avatar">
        <i class="material-icons circle stop-color">
        <i class="fa fa-minus-circle"></i>
        </i>
        <div class="list-info stop">
        <span class="title">Bradford, Sam</span>
        <p class="grey-text left-align">Termed from</p>
        <p class="grey-text left-align">The Philadelphia Eagles</p>
        </div>
        <div class="secondary-content">
        <p class="grey-text right-align">1/2/17</p>
        <i class="fa fa-chevron-right right"></i>
        </div>
      </li>
    </ul>
  </div>
  </div>

  <div class="col s12 m4">
    <div class="eventContainerFront">
      <ul class="collection"> 
      <li class="collection-item avatar">
        <i class="material-icons circle start-color">
        <i class="fa fa-bell-o"></i>
        </i>
        <div class="list-info start">
        <span class="title">Kilpatrick, Brian</span>
         <p class="grey-text left-align">Starting @</p>
        <p class="grey-text left-align">The Judge Group</p>
        </div>
        <div class="secondary-content">
        <p class="grey-text right-align">8/27/16</p>
        <i class="fa fa-chevron-right right"></i>
        </div>
      </li>
  
      <li class="collection-item avatar">
        <i class="material-icons circle stop-color">
        <i class="fa fa-minus-circle"></i>
        </i>
        <div class="list-info stop">
        <span class="title">Bradford, Sam</span>
        <p class="grey-text left-align">Termed from</p>
        <p class="grey-text left-align">The Philadelphia Eagles</p>
        </div>
        <div class="secondary-content">
        <p class="grey-text right-align">1/2/17</p>
        <i class="fa fa-chevron-right right"></i>
        </div>
      </li>
    </ul>
  </div>
  </div>

  <div class="col s12 m4">
  <div class="eventContainerFront">
  <ul class="collection">
    <li class="collection-item avatar">
      <i class="material-icons circle meeting-color">
      <i class="fa fa-clock-o"></i>
      </i>
      <div class="list-info meeting">
      <span class="title">Myers, Ronnie</span>
      <p class="secondary-text">Meeting in 3 hours</p>
      <p>Notes: I think this cand...</p>
      </div>
      <div class="secondary-content">
      <p class="grey-text right-align">8/16/16</p>
      <i class="fa fa-chevron-right right"></i>
      </div>
    </li>
  
  <li class="collection-item avatar">
      <i class="material-icons circle submittal-color">
      <i class="fa fa-arrow-right"></i>
      </i>
      <div class="list-info submittal">
      <span class="title">Smith, John</span>
      <p class="secondary-text">J. Smith to D. Avera</p>
      <p>Five Star Gourmet Foods</p>
      </div>
      <div class="secondary-content">
      <p class="grey-text right-align">8/16/16</p>
      <i class="fa fa-chevron-right right"></i>
      </div>
  </li>
</ul>  
</div>
</div>
</div>
</div>

