<div class="section col s12 ">
  <br>
  <div class="row">
 <?php foreach($business as $bus):?> 
    <div class="col s12 m3">
      <div class="card-panel" style="background-color: rgb(0,0,0);
  background-color: rgba(0,0,0, 0.9);">
       <?php 
        echo '<img src="'.base_url().'/uploads/'.$bus->b_img.'" class="img-responsive" width="100%">';      
        ?>     
      </div>
<div class="card-panel">
  <a href="#" class="grey-text text-darken-3">
    <i class="material-icons left grey-text mb-2">email</i>
      <?php echo $bus->b_email;?>
      </a>
      <br><hr><br>

        <a href="#" class="grey-text text-darken-3"><i class="material-icons left grey-text">phone</i>
      <?php echo $bus->b_phone;?> 
      </a>
      <br><hr><br>
        <a href="#" class="grey-text text-darken-3"><i class="material-icons left grey-text">location_on</i><?php echo $bus->b_location;?><br>          
        </a>        
      </div>      
    </div>    
       
  


<div class="container col s12 m8" style="background-color: rgb(0,0,0);
  background-color: rgba(0,0,0, 0.8);padding: 1.5rem;">
<div class="row">
  <div class="col s12 container">   
   <?php 
   echo '     
          
<div class="row">          
  <div class="card-content grey-text">
    <h3 class="header left-align">'.ucfirst($bus->b_name).'<br>
      <span class="grey-text text-lighten-1 small-text left-align">
        '.$bus->b_location.'
      </span>
    </h3>
    <br>

   <p class="left-align grey-text"> '.$bus->b_bio.'</p>
   <a href="#modal1" class="btn grey-text text-darken-2 indigo lighten-5 modal-trigger">
    <i class="material-icons">menu</i>view our products
    </a>
  
 </div>
</div> 
</div>';    
  endforeach;?>
<br>
  
 </div>
</div>

</div>
<br><br>  
         
 

  <!-- Modal Structure -->
  <div id="modal1" class="modal bottom-sheet">
    <div class="modal-content">      
    <div class="col s12">
    <div class="row">
     <?php foreach($services as $service){
       echo '
       <div class="col s6 m4"> 
        <div class="card">
         <div class="card-image"> 
        <img class="responsive-img" width="100%" src="'.base_url().'uploads/'.$service->img2.'">
           </div>
           <div class="card-content grey lighten-4">
        <span class="card-title">'.$service->s_name.'</span>    
         <a href="'.base_url().'service/'.$service->s_id.'" class="btn grey darken-2">view</a> 
     </div>
     </div>
    </div>';
  }?>
<!-- end card-tree -->
 </div >
 </div >
</div>

    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">close</a>
    </div>
    
    </div>   
  </div>
</div>




      <!-- Compiled and minified JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>/jquery/jqueryc.js"></script>  
<script type="text/javascript" src="<?php echo base_url();?>/jquery/menu.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>jquery/steps/step.custom.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>jquery/scroll.js"></script>
</body>
  </html>