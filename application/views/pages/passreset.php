<div class="section"></div>
<div class="section"></div>
  
<center>     
<div class="container">
 <div class="z-depth-1 grey lighten-4 row" style="display: inline-block; padding: 32px 48px 0px 48px; border: 1px solid #EEE;">
<div class="grey p2"><?php echo validation_errors();?></div>
<?php echo form_open("passreset");?>           
<h5 class="prim">Password Reset</h5>
 <br>
  <div class='row'>
    <div class='input-field col s12'>
      <input class='validate'type='password' autocomplete="password" name='password' id='password' required />
        <label class="active" for='password'>Enter new password</label>
    </div>
    <input type="hidden" name="id" value="<?php echo $this->uri->segment(2)?>">
  </div>

<div class='row'>
  <div class='input-field col s12'>
    <input class='validate' autocomplete="passconfirm" type='password' name='passconfirm' id='passconfirm' required />
      <label class="active" for='passconfirm'>confirm password</label>
  </div>              
</div>

<br />
  <center>
    <div class='row'>
     <button type='submit' name='btn_login' class='col s12 btn btn-large waves-effect red'>Send
     </button>

      <center>
        <b>Not registered ?</b>
         <br>
        </b>
        <a href="<?php echo base_url('register'); ?>" class="grey-text">Register here</a>
      </center>
    </div>
 </center>
          
</form>
</div>
</div>      
</center>

    <div class="section"></div>
    <div class="section"></div>
<!-- Compiled and minified JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>/jquery/jqueryc.js"></script>  
<script type="text/javascript" src="<?php echo base_url();?>/jquery/menu.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>jquery/steps/step.custom.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>jquery/scroll.js"></script>
  
  </body>
</html>
