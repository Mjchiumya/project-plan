  <div class="container white">
   <?php foreach($services as $serv):?>
    <div class="row">
        <div class="col s12">
          <div class="card">
            <div class="card-image">
              <img src="<?php echo base_url().'uploads/'.$serv->img2;?>" class="materialboxed">             
            </div>           
          </div>
        </div>
    
      <div class="row">
        <div class="col s4 m4">
          <div class="card">
            <div class="card-image">
              <img src="<?php echo base_url().'uploads/'.$serv->img2;?>" class="materialboxed">             
            </div>           
          </div>
        </div>

        <div class="col s4 m4">
          <div class="card">
            <div class="card-image">
               <img src="<?php echo base_url().'uploads/'.$serv->img3;?>" class="materialboxed">             
            </div>           
          </div>
        </div>

        <div class="col s4 m4">
          <div class="card">
            <div class="card-image">
                <img src="<?php echo base_url().'uploads/'.$serv->img1;?>" class="materialboxed">             
            </div>           
          </div>
        </div>

      </div>
   </div>
<span class="orange-text"><i class="material-icons">star</i></span>
<span class="orange-text"><i class="material-icons">star</i></span>
<span class="orange-text"><i class="material-icons">star_half</i></span>
<span class="orange-text"><i class="material-icons">star_border</i></span>
<span class="orange-text"><i class="material-icons">star_border</i></span>
<div class="section">
  <div class="row">
        <div class="col s12">          
          <?php 
             echo  '                                
               <h4>
                 <span class="card-title grey-text text-darken-1">'.ucwords($serv->s_name).'       
                 </span>
              </h4> 

              <span class="center-align grey-text text-darken-2">
                 <b>K'.$serv->s_price.'</b>
                <span href="#" class="right grey-text text-darken-2 btn-flat">
                   <i class="material-icons">category</i>
                    '.$serv->s_category.'
                </span></span>             
               <br><br>

              <p class="left-align grey-text">
              <i>'.trim($serv->s_info).'<i>
              </p>
            
            <div class="card-action">             
              <a href="'.base_url().'business/'.$serv->b_id.'" class="btn grey lighten-1  grey-text text-darken-2">
                 <i class="material-icons left">person</i>
                  made by
             </a>
          </div>
            ';?>
   <?php endforeach;?>
      
        </div>
      </div>  
   
</div>

    
      <!-- Compiled and minified JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>/jquery/jqueryc.js"></script>  
<script type="text/javascript" src="<?php echo base_url();?>/jquery/menu.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>jquery/steps/step.custom.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>jquery/scroll.js"></script>
</body>
  </html>

  
