<div class="container">
  <br>
   <div class="progress">
      <div class="determinate" id="bar" ></div>
  </div>
 
 <div class="card-content red"><?php echo validation_errors();?></div>
 <?php echo form_open('admin-business-add'); ?>

  <div id="account_details" class="qontainer">
    <p class='form_head grey-text text-darken-3'>Business ID</p> 
      
    <br>
      <div class="row">
        <div class="input-field col s6">
          <input id="name" type="text" class="validate" name="name" required>
          <label for="name">Name</label>
        </div>
        <div class="input-field col s6">
          <input id="location" type="text" class="validate" name="location" required>
          <label for="location">City/Town</label>
        </div>
      </div>
      <div class="row">
          <div class="input-field col s12">
            <textarea id="bio" class="materialize-textarea" name="bio" data-length="120"></textarea>
            <label for="bio">Bio</label>
          </div>
        </div>  

  <a title="Login" class="ngl btn-floating btn-large waves-effect waves-light red"><i class="material-icons">input</i>
  </a>

    <br>
    <input type="button" class="blue-grey lighten-3" value="Next" onclick="show_next('account_details','contact_details','bar');">   
  </div>


  <div id="contact_details" class="qontainer">
    <p class='form_head'>Contact Details</p>
    <div class="row">
        <div class="input-field col s6">
          <input id="phone" type="text" class="validate" name="phone" required>
          <label for="phone">Phone</label>
        </div>
        <div class="input-field col s6">
          <input id="email" type="text" class="validate" name="email" required>
          <label for="email">Email</label>
        </div>
      </div>
         
    <input type="button" class="blue-grey lighten-3" value="Previous" onclick="show_prev('account_details','bar');">
    <input type="button" class="blue-grey lighten-3" value="Next" onclick="show_next('contact_details','extra_details','bar');">
  </div>
        
  <div id="extra_details" class="qontainer">
    <p class='form_head'>Extra Details</p>
     <div class="row">
       <div class="input-field col s12">
        <input id="openTime" type="text" name="openTime">
          <label for="openTime">business hours/days (eg 9:00 - 17:00 Mon-Fri )</label>
        </div>
        </div>  
    <input type="button" class="blue-grey lighten-3" value="Previous" onclick="show_prev('contact_details','bar');">
    <input type="Submit" class="blue-grey lighten-3" value="Submit">
  </div>
  </form>
  
</div>