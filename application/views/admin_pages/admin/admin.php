<div class="row">

 <div class="col m3 s12">
     
    <ul id="slide-out" class="side-nav fixed">
      <br>
      <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li>
            <a class="collapsible-header black-text active">Users<i class="material-icons">arrow_drop_down</i></a>
            <div class="collapsible-body">
              <ul>
                <?php foreach($ucount as $key => $value):?>
                <li><a href="#!" class="grey-text text-darken-1"><?php echo ucfirst($key) ." "." (".$value." )";?></a></li>                
              <?php endforeach;?>
              </ul>
            </div>
          </li>
        </ul>
      </li>

      <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li>
            <a class="collapsible-header black-text active">Service<i class="material-icons">arrow_drop_down</i></a>
            <div class="collapsible-body">
              <ul>
                <?php foreach($scount as $key => $value):?>
                <li><a href="#!" class="grey-text text-darken-1">
                     <?php echo ucfirst($key) ." "." (".$value." )";?>                       
                     </a>
                </li>                
              <?php endforeach;?>
              </ul>
            </div>
          </li>
        </ul>
      </li>

      <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li>
            <a class="collapsible-header black-text active">Business<i class="material-icons">arrow_drop_down</i></a>
            <div class="collapsible-body">
              <ul>
                <?php foreach($bcount as $key => $value):?>
                <li><a href="#!" class="grey-text text-darken-1"><?php echo ucfirst($key) ." "." (".$value." )";?></a></li>                
              <?php endforeach;?>
              </ul>
            </div>
          </li>
        </ul>
      </li>
    </ul>    
 </div> 
 
  <div class="col m9 s12">
  <div class= "section">
    <div class="row">
      <div class="col s12 m6">
        <div style="padding: 2.5rem;" align="center" class="card">
          <div class="row">
            <div class="left card-title">
              <b>User Management</b>
            </div>
          </div>

          <div class="row">
            <a href="<?php base_url();?>admin-user-view">
              <div style="padding: 1.5rem;" class="grey lighten-3 col s5 waves-effect">
                <i class="grey-text text-darken-2 large material-icons">person</i>
                <span><h5 class="grey-text text-darken-2">Users</h5></span>
                
              </div>
            </a>
            <div class="col s1">&nbsp;</div>
            <div class="col s1">&nbsp;</div>

            <a href="#!">
              <div style="padding: 1.5rem;" class="grey lighten-3 col s5 waves-effect">
                <i class="grey-text text-darken-2 large material-icons">people</i>
                 <span><h5 class="grey-text text-darken-2">Admin</h5></span>
              </div>
            </a>
          </div>
        </div>
      </div>

      <div class="col s12 m6">
        <div style="padding: 2.5rem;" align="center" class="card">
          <div class="row">
            <div class="left card-title">
              <b>Product Management</b>
            </div>
          </div>
          <div class="row">
            <a href="<?php base_url();?>admin-service-view">
              <div style="padding: 1.5rem;" class="grey lighten-3 col s5 waves-effect">
                <i class="grey-text text-darken-2 large material-icons">redeem</i>
                 <span>
                    <h5 class="grey-text text-darken-2">                      
                        Services                   
                    </h5>
                 </span>
              </div>
            </a>

            <div class="col s1">&nbsp;</div>
            <div class="col s1">&nbsp;</div>

            <a href="#!">
              <div style="padding: 1.5rem;" class="grey lighten-3 col s5 waves-effect">
                <i class="grey-text text-darken-2 large material-icons">add_circle_outline</i>
                 <span><h5 class="grey-text text-darken-1">Add Service</h5></span>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col s12 m6">
        <div style="padding: 2.5rem;" align="center" class="card">
          <div class="row">
            <div class="left card-title">
              <b>Brand Management</b>
            </div>
          </div>

          <div class="row">
            <a href="<?php base_url();?>admin-business-view">
              <div style="padding: 1.5rem;" class="grey lighten-3 col s5 waves-effect">
                <i class="grey-text text-darken-2 large material-icons">business</i>
                 <span><h5 class="grey-text text-darken-2">                   
                   Business                  
                 </h5>
               </span>
              </div>
            </a>

            <div class="col s1">&nbsp;</div>
            <div class="col s1">&nbsp;</div>

            <a href="admin-business">
              <div style="padding: 1.5rem;" class="grey lighten-3 col s5 waves-effect">
                <i class="grey-text text-darken-2 large material-icons">add_circle_outline</i>
                 <span><h5 class="grey-text text-darken-2">Add Business</h5></span>
              </div>
            </a>
          </div>
        </div>
      </div>

      <div class="col s12 m6">
        <div style="padding: 2.5rem;" align="center" class="card">
          <div class="row">
            <div class="left card-title">
              <b>Event Management</b>
            </div>
          </div>
          <div class="row">
            <a href="<?php base_url();?>admin-orders">
              <div style="padding: 1.5rem;" class="grey lighten-3 col s5 waves-effect">
                <i class="grey-text text-darken-2 large material-icons">add_shopping_cart</i>
                <span><h5 class="grey-text text-darken-2">Orders</h5></span>
              </div>
            </a>
            <div class="col s1">&nbsp;</div>
            <div class="col s1">&nbsp;</div>

            <a href="#!">
              <div style="padding: 1.5rem;" class="grey lighten-3 col s5 waves-effect">
                <i class="grey-text text-darken-2 large material-icons">view_list</i>
                 <span><h5 class="grey-text text-darken-2">Events</h5></span>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>

    <div class="fixed-action-btn click-to-toggle" style="bottom: 45px; right: 24px;">
      <a class="btn-floating btn-large red waves-effect waves-light">
        <i class="large material-icons">add</i>
      </a>

      <ul>
        <li>
          <a class="btn-floating red"><i class="material-icons">business</i></a>
          <a href="admin-business" class="btn-floating fab-tip">Add a business</a>
        </li>

        <li>
          <a class="btn-floating yellow darken-1"><i class="material-icons">people</i></a>
          <a href="" class="btn-floating fab-tip">Add administrator</a>
        </li>

        <li>
          <a class="btn-floating green"><i class="material-icons">redeem</i></a>
          <a href="" class="btn-floating fab-tip">Add a service</a>
        </li>
        
      </ul>
    </div>
</div>
</div>
</div>
 