<div class="container">
    <div class="card-panel"> 
    <!-- Dropdown Trigger -->
           
  <a class='dropdown-button btn btn-floating pulse amber darken-2' href='#' data-activates='dropdown2'>
    <i class="material-icons white-text left">expand_more</i>
  </a>

  <!-- Dropdown Structure -->
  <ul id='dropdown2' class='dropdown-content'>
    
    <?php foreach ($services as $s) {

        if ($s->s_status == 0) {
             echo'
                 <li><a href="'.base_url().'/admin-service-activate/'.$s->s_id.'" class="grey-text text-darken-2">
                  <i class="material-icons grey-text text-darken-2">lock_open</i>
                  Activate
                </a></li>';
            }else{
             echo '   <li>
                     <a href="'.base_url().'admin-service-deactivate/'.$s->s_id.'"  class="grey-text text-darken-2">
                      <i class="material-icons grey-text text-darken-2">lock</i>
                       Deactivate</a></li>';
               }
             echo '
                 <li><a href="'.base_url().'admin-bedit/'.$s->b_id.'" class="grey-text text-darken-2"> 
                     <i class="material-icons grey-text text-darken-2">contacts</i>  
                     View Owner
                   </a></li>'.
                '<li><a href="'.base_url().'admin-service-delete/'.$s->s_id.'" class="grey-text text-darken-2">
                 <i class="material-icons grey-text text-darken-2">delete_forever</i>
                  Delete
                 </a></li>';     
             }?>
       
   </ul>         
 </div> 
</div>


 
<div class="container">
  <div class="row">
    <?php 
          $imgNo = 0;
             foreach($services as $key){      
          echo '
            <div class="col s12 m4">
             <div class="card">
              <div class="card-image">
              <img src="'.base_url().'/uploads/'.$key->img1.'" alt="image '.$imgNo++.' perfect-plan" class="materialboxed" width="100%"></div> 
              <div class="card-content">
               <a href="'. base_url("product-image-upload").'/'.$imgNo.'/'.$key->s_id.'" class="btn btn-floating grey darken-1 white-text">
            <i class="material-icons left">cloud_upload</i>upload</a>
              </div>   
            </div></div>'.

            '<div class="col s12 m4">
              <div class="card">
               <div class="card-image">
                 <img src="'.base_url().'/uploads/'.$key->img2.'" alt="image '.$imgNo++.' perfect-plan" class="materialboxed" width="100%">
                 </div>
                 <div class="card-content">
                    <a href="'. base_url("product-image-upload").'/'.$imgNo.'/'.$key->s_id.'" class="btn btn-floating grey darken-1 white-text">
                     <i class="material-icons left">cloud_upload</i>upload</a> 
                 </div>                 
            </div></div>'.

            '<div class="col s12 m4">
              <div class="card">
               <div class="card-image">    
              <img src="'.base_url().'/uploads/'.$key->img3.'" alt="image '.$imgNo++.' perfect-plan" class="materialboxed" width="100%"></div>
              <div class="card-content">
              <a href="'. base_url("product-image-upload").'/'.$imgNo.'/'.$key->s_id.'" class="btn btn-floating grey darken-1 white-text">
            <i class="material-icons left">cloud_upload</i>upload</a>
            </div>    
            </div></div>';
            }?>
     </div>
</div>

<div class="container">
<?php echo form_open('seditor'); ?>  
<div class="col s10" style="background-color: rgb(0,0,0);
  background-color: rgba(0,0,0, 0.6);padding: 1rem;">
    <form class="col s10">
        <?php
            foreach($services as $serv){
               echo'             
            <input type="hidden" name="bid" class="validate"
                  value="'.$serv->s_id.'" style="background-color:blue;">             
             
            <div class="row">               
               <div class="input-field col s6">
                <i class="material-icons prefix">visibility</i>
                <input type="text" name="name" class="validate"
                  value="'.$serv->s_name.'">             
                </div>

               <div class="input-field col s6">
               <i class="material-icons prefix">sort</i>
                 <input name="category" class="validate" type="text"
                 value="'.$serv->s_category.'">              
              </div>
           </div>

         <div class="row">
            <div class="input-field col s6">
             <i class="material-icons prefix">room</i>
              <input type="text" name="demography" class="validate"
                  value="'.$serv->s_demography.'">             
            </div>
            <div class="input-field col s6">
            <i class="material-icons prefix">monetization_on</i>
             <input name="price" class="validate" type="text"
                 value="'.$serv->s_price.'">               
           </div>
         </div>         
         
         <div class="row">   
            <div class="input-field col s12">
            <i class="material-icons prefix">message</i>
             <input name="info" class="validate" type="text"
                 value="'.$serv->s_info.'">              
           </div>
         </div>
            
           <input type="submit" name="submit" class="btn btn-primary red" value="Submit"/>
           </div>
       '; 
   }?>            
 </form>
<hr><br>
</div>

  

