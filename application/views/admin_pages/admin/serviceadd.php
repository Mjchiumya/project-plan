<div class="container">
  <br>
   <div class="progress">
      <div class="determinate" id="bar" ></div>
  </div>
 
 <div class="card-content red"><?php echo validation_errors();?></div>
 <?php echo form_open('admin-service-add'); ?>

  <div id="account_details" class="qontainer">
    <p class='form_head '>Service Details</p><br> 
      
    <br>
       <input type="hidden" name="id" value="<?php echo $this->uri->segment(2);?>">
      <div class="row">
        <div class="input-field col s6">
          <input id="name" type="text" class="validate" name="name" required>
          <label for="name">Name</label>
        </div>
        <div class="input-field col s6">
          <input  type="text" class="validate" name="category" required>
          <label for="category">category</label>
        </div>
      </div>
      
      

  <a title="Login" class="ngl btn-floating btn-large waves-effect waves-light red"><i class="material-icons">input</i>
  </a>

    <br>
    <input type="button" class="blue-grey lighten-3" value="Next" onclick="show_next('account_details','contact_details','bar');">   
  </div>


  <div id="contact_details" class="qontainer">
    <p class='form_head'>Market Details</p>
    <div class="row">
        <div class="input-field col s6">
          <input id="demography" type="text" class="validate" name="demography" required>
          <label for="demography">name of area you do business</label>
        </div>
        <div class="input-field col s6">
          <input id="price" type="text" class="validate" name="price" required>
          <label for="price">Price</label>
        </div>
      </div>
         
    <input type="button" class="blue-grey lighten-3" value="Previous" onclick="show_prev('account_details','bar');">
    <input type="button" class="blue-grey lighten-3" value="Next" onclick="show_next('contact_details','extra_details','bar');">
  </div>
        
  <div id="extra_details" class="qontainer">
    <p class='form_head'>Product Summary</p>
     <div class="row">
          <div class="input-field col s12">
            <textarea id="bio" class="materialize-textarea" name="bio" data-length="200"></textarea>
            <label for="bio">Bio</label>
          </div>
        </div>   
    <input type="button" class="blue-grey lighten-3" value="Previous" onclick="show_prev('contact_details','bar');">
    <input type="Submit" class="blue-grey lighten-3" value="Submit">
  </div>
  </form>
  
</div>