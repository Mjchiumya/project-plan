<div class="section">
  <div class="row">
 <div class="row">
    <div class="col s12">
      <div class="row">
        <div class="input-field col s12">
          <i class="material-icons prefix">textsms</i>
          <input type="text" id="autocomplete-input" class="autocomplete">
          <label for="autocomplete-input">Autocomplete</label>
        </div>
      </div>
    </div>
  </div>
        




    
    <div class="col s12 m12 white"> 
       <div class="col m6">
        <form method="post">              
            <div class="form-group">
                <div class="col m6 input-field">                 
                   <i class="material-icons prefix grey-text">search</i>
                    <input class="form-control" id="search" name="search" type="text" />
                </div>             

                <div class="col m6">
                 <input id="btn" name="btn_search" type="submit" class="btn red" value="Search" />
                 <a href="<?php echo base_url()?>admin-business-view" class="btn red">Show All</a>
                </div>
            </div>
        </form>
  </div>
</div>
    
 
 <div class="row">
<?php foreach($business->result() as $bus ) { 

  echo '
 <div class="col s6 m4 "> 
  <div class="card small " >
   <div class="card-image waves-effect waves-block waves-light" >
     <img class="img-responsive" src="' . base_url() . 'uploads/'. $bus->b_img . '" width="100%">    
   </div >
  <div class="card-content" >
     <a href="'.base_url(). 'admin-bedit/'.$bus->b_id.'"><span class="card-title truncate grey-text text-darken-2">' . ucfirst($bus->b_name) . '</span></a>
     <span class=" activator grey-text text-darken-4" > '.$bus->b_location.'</span>
  </div>
</div>
</div>
';
      }
  
  ?>
</div>
            

     
<div class="row">
 <div class="col m8 center-block">
   <ul class="pagination center-align">
    <?php echo $this->pagination->create_links(); ?>
  </ul>
 </div>
</div>

</div>
</div>
</body>
</html>