<div class="section col s12" >
  <br>
  <div class="row">
    <div class="col s12 m3">
    <div class="card-panel" style="background-color: rgb(0,0,0);
      background-color: rgba(0,0,0, 0.9);">
     <div class="center">
       <?php foreach ($business as $key){
          if($key->b_img === 'null'){
            echo'<h1 class="header white-text"><br>
          No Logo uploaded</h1>';  
        }else{
        	  echo '<img src="'.base_url().'/uploads/'.$key->b_img.'" class="circle" style="width:14rem;">
        	  <h3 class="grey-text text-lighten-4">'.$key->b_name.'<span class="card-header white-text"><br>
              '.$key->b_location.'</span></h3>';
             }
       }?>
      </div>      
      </div>

 <div class="card-panel">
 	<ul>
 		<?php  foreach ($business as $key ) {
 			echo'
      <li class="waves-effect waves-light">
         <a href="'. base_url("logo-upload").'/'.$key->b_id.'" class="btn grey darken-1 white-text">
            <i class="material-icons left">cloud_upload</i>Logo</a>
      </li>
      <li class="waves-effect waves-light">
         <a href="'. base_url("admin-service-add").'/'.$key->b_id.'" class="btn grey darken-1 white-text">
            <i class="material-icons left">add_circle_outline</i>add service</a>
      </li>
       ';}?>
    </ul>
  
 </div> 

<div class="card-panel">
  
  <ul id="slide-out" class="side-nav fixed">      
      <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li>
            <a class="collapsible-header grey-text text-darken-2">Active Service<i class="material-icons">arrow_drop_down</i></a>
            <div class="collapsible-body">
              <ul>
                <?php foreach ($active as $value) {
                	echo '<li><a href="'.base_url().'admin-sedit/'.$value->s_id.'" class="grey-text text-darken-2">'.$value->s_name.'</a></li>';
                } ?>                                
             
              </ul>
            </div>
          </li>
        </ul>
      </li>
  </ul>
  <ul id="slide-out" class="side-nav fixed">      
      <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li>
            <a class="collapsible-header grey-text text-darken-2">Inactive Service<i class="material-icons">arrow_drop_down</i></a>
            <div class="collapsible-body">
              <ul>
                
               <?php foreach ($inactive as $val) {
                	echo '<li><a href="'.base_url().'admin-sedit/'.$val->s_id.'" class="grey-text text-darken-2">'.$val->s_name.'</a></li>';
                } ?>                                
                       
             
              </ul>
            </div>
          </li>
        </ul>
      </li>
  </ul>
</div>

    
</div>    
       
 <div class="container col s12 m8">
    <div class="card-panel"> 
    <!-- Dropdown Trigger -->
           
  <a class='dropdown-button btn btn-floating pulse amber darken-2' href='#' data-activates='dropdown2'>
    <i class="material-icons white-text left">expand_more</i>
  </a>

  <!-- Dropdown Structure -->
  <ul id='dropdown2' class='dropdown-content'>
    <li>
        <a href="<?php $id=$this->uri->segment(2);echo base_url("admin-delete-allservices").'/'.$id;?>" class="grey-text text-darken-2">
          <i class="material-icons left red-text">delete_forever</i>Dellete all Services
        </a>
     </li>

      <li><a href="<?php $bid=$this->uri->segment(2);echo base_url("admin-delete-business").'/'.$bid;?>" class="grey-text text-darken-2"><i class="material-icons left red-text">delete_forever</i>Delete Business</a>
      </li>     
   </ul>         
 </div> 
</div>
 


<div class="container col s12 m8" style="background-color: rgb(0,0,0);
  background-color: rgba(0,0,0, 0.5);">


  <div class="section">
  	<div class="center card-title white-text">
              <h3 ><b>Business Editor </b></h3>
      </div>

<?php echo form_open('beditor'); ?>	
  <div class="row">
       <form class="col s12">
       	  <?php
          	foreach($business as $bus){
          	   echo'
          	 
                <input type="hidden" name="bid" class="validate"
                  value="'.$bus->b_id.'" style="background-color:blue;">             
            </div>  
            <div class="row">          	   	
              <div class="input-field col s6">
                <input type="text" name="name" class="validate"
                  value="'.$bus->b_name.'">
             
            </div>

            <div class="input-field col s6">
             <input name="location" class="validate" type="text"
                value="'.$bus->b_location.'">
              
           </div>
         </div>

         <div class="row">
            <div class="input-field col s6">
              <input type="text" name="phone" class="validate"
                  value="'.$bus->b_phone.'">
             
            </div>
            <div class="input-field col s6">
             <input name="email" class="validate" type="text"
                 value="'.$bus->b_email.'">
               
           </div>
         </div>

         <div class="row">
            <div class="input-field col s12">
              <input type="text" name="hours" class="validate"
                value="'.$bus->b_openhrs.'">
             
            </div>
         </div>
         <div class="row">   
            <div class="input-field col s12">
             <input name="bio" class="validate" type="text"
                 value="'.$bus->b_bio.'">  
               
           </div>
         </div>
     </div>       
           <input type="submit" name="submit" class="btn btn-primary red" value="Submit"/>
       '; 
   }?>            
 </form>		
</div>






</div>
  
</div>      
    

  </div>
</div>

