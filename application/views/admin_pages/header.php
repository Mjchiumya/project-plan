<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>   
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>/css/main.css">
    <link rel="stylesheet" href="<?php echo base_url();?>/css/admin.css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu+Condensed" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="https://unpkg.com/tabulator-tables@4.1.4/dist/css/tabulator.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <title>Perfect Plan</title>
</head>
<body>

<!-- Dropdown Structure -->
<ul id="dropdown1" class="dropdown-content">
    <li><a class="grey-text text-darken-2" href="<?php echo base_url();?>admin">Admin Home</a></li>
    <li><a class="grey-text text-darken-2" href="<?php echo base_url();?>admin-service-view">Service</a></li>
    <li><a class="grey-text text-darken-2" href="<?php echo base_url();?>admin-business-view">Business</a></li>
    <li><a class="grey-text text-darken-2" href="#user">Users</a></li>
    <li class="divider"></li>
    <li><a class="grey-text text-darken-2" href="<?php echo base_url();?>">website</a></li>
</ul>
<nav>
  <div class="nav-wrapper  darken-3">   
    <ul class="right">
      <li><a href="sass.html">Sass</a></li>
      <li><a href="badges.html">Components</a></li>
      <!-- Dropdown Trigger -->
      <li><a class="dropdown-button" href="#!" data-activates="dropdown1">Menu<i class="material-icons right">arrow_drop_down</i></a></li>
    </ul>
  </div>
</nav>
