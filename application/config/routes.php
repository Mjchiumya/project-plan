<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//pages
$route['default_controller'] = 'PagesController';
$route['home'] = 'PagesController/index';
$route['coming'] = 'PagesController/comingsoon';
$route['login']='PagesController/login_view';
$route['register']='PagesController/register_view';
$route['user']='AuthController/user_profile';
$route['email']='EmailController/index';
//password resets
$route['forgot']='PagesController/forgotpass_view';
$route['emailme'] = 'EmailController/recover';
$route['passreset'] = 'MyController/resetPassword';
//auth
$route['register_user']='AuthController/register_user';
$route['login_user']='AuthController/login_user';
$route['logout']='AuthController/user_logout';
$route['deactivate/(:num)']='AuthController/user_deactivate/$1';
$route['reset/(:num)']='PagesController/passresetview/$1';
//business
$route['service/(:num)']='PagesController/serviceview/$1';
$route['business/(:num)']='PagesController/businessview/$1';
$route['cancel-order/(:num)']='MyController/cancelOrder/$1';
$route['order/(:num)'] = 'EmailController/sendOrder/$1';
//event
$route['create']='EventController/create_view';
$route['eventprofile']='EventController/event_input';
$route['editevent/(:num)']='EventController/event_editor/$1';
$route['deleteevent/(:num)']='EventController/del_event/$1';
$route['event/(:num)']='EventController/eventview/$1';
$route['live-event/(:num)']='EventController/liveEventView/$1';
$route['form']='EventController/event_form';
$route['formevtedit']='EventController/evtedit_formval';
//cart
$route['addservice/(:num)']='CartController/create_cart/$1';
$route['addservice/(:num)/(:num)']='CartController/create_cart/$1';
$route['cartadd']='cartController/addToCart';
$route['cartitemdel']='CartController/removeItem';
$route['cartload']='CartController/loadCart';
$route['clear']='CartController/clearCart';
$route['eventreg/(:num)']='CartController/cartdb/$1';

//*******BACKEND URL*********

$route['admin'] ='AdminPagesController/adminIndex';
$route['logoset'] = 'AdminController/logoUpload';
$route['logo-upload/(:num)'] ='AdminPagesController/addLogo/$1';
$route['admin-user-view'] ='AdminPagesController/userview';
$route['adminusers'] ='AdminController/adminusers';
//admin order
$route['admin-orders'] ='AdminPagesController/adminOdersView';
$route['adminorders'] ='AdminController/adminOrdersJson';

//admin business
$route['admin-bedit/(:num)'] ='AdminPagesController/businessEditor/$1';
$route['beditor'] ='AdminController/businessEditorForm';
$route['admin-business-view'] ='FilterController/index';
$route['admin-business-edit'] ='AdminPagesController/businessEditor';
$route['admin-business-view/(:num)'] ='FilterController/index';
$route['admin-business'] ='AdminPagesController/addBusiness';
$route['admin-business-add'] ='AdminController/addBusiness';

//admin service
$route['admin-sedit/(:num)'] ='AdminPagesController/serviceEditor/$1';
$route['admin-service-activate/(:num)'] ='AdminController/activateServiceStatus';
$route['admin-service-deactivate/(:num)'] ='AdminController/deactivateServiceStatus';

//deletes
$route['admin-delete-allservices/(:num)'] ='AdminController/deleteAllServices';
$route['admin-delete-business/(:num)'] ='AdminController/deleteBusiness';

$route['seditor'] ='AdminController/serviceEditorForm';
$route['admin-service'] ='AdminPagesController/service';
$route['admin-service-view'] ='FilterController/serviceFilter';
$route['admin-service-view/(:num)'] ='FilterController/serviceFilter';
$route['admin-service-add'] = 'AdminController/addService';
$route['admin-service-add/(:num)'] ='AdminPagesController/addService';
$route['product-image-upload/(:num)/(:num)'] ='AdminPagesController/uploadProductImage/$1';
$route['product-image-upload/(:num)/(:num)'] ='AdminPagesController/uploadProductImage/$1';
$route['product-image-upload/(:num)/(:num)'] ='AdminPagesController/uploadProductImage/$1';
$route['productimagesend/(:num)/(:num)'] = 'AdminController/imageUpload';
//backend json
$route['adminCategory']='AdminController/serviceCategories';
$route['admin-busjson']='AdminController/adminBusjson';
//security/404
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
