function checkLevelUp(bar){
  var value = window.getComputedStyle(bar);
   var widthValue = value.getPropertyValue('width');
     if(widthValue == '100px' ){
        bar.style.width = "250px";

        }else{
      bar.style.width = "460px";
     }
}

function checkLevelDown(bar){
  var value = window.getComputedStyle(bar);
  var widthValue = value.getPropertyValue('width');
   if(widthValue == '560px' ){
       bar.style.width = "250px";      
   }else{
      bar.style.width = "100px";
    }
}

function show_next(id,nextid,bar)
{
  var ele=document.getElementById(id).getElementsByTagName("input"); 
 
  var error=0;
  for(var i=0;i<ele.length;i++)
  {
    if(ele[i].type=="text" && ele[i].value=="")
  {
    error++;
  }
  }
    
  if(error==0)
  {
    document.getElementById("account_details").style.display="none";
    document.getElementById("contact_details").style.display="none";
    document.getElementById("extra_details").style.display="none";
    $("#"+nextid).fadeIn();
    //document.getElementById(bar).style.width="45%";
    checkLevelUp(document.getElementById(bar));

  }
  else
  {
    alert("Fill All The details");
  }
}

function show_prev(previd,bar)
{
  document.getElementById("account_details").style.display="none";
  document.getElementById("contact_details").style.display="none";
  document.getElementById("extra_details").style.display="none";
  $("#"+previd).fadeIn();
  //document.getElementById(bar).style.width="75%";
  checkLevelDown(document.getElementById(bar));
}

