$(document).ready(function(){
	var url = window.location.origin;
	$('#orders').DataTable({
        "processing": true,
        "serverSide": false,
        "pagingType":"full",
        "dom": '<"toolbar">frtip',
        "type":"POST",
        "ajax": {
          "url":url+'/perfect-plan/adminorders',"dataSrc":"",
          "type":"POST"},

         "columns": [
            { "data": "o_id" },
            { "data": "e_name" },
            { "data": "u_name" },
            { "data": "e_date" },
            { "data": "e_status" }
          ] 
      });
    
    $("div.toolbar").html('<b>All Orders</b>');
});