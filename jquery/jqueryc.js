$(document).ready(function () {  
	$(".dropdown-button dropdown-btn").dropdown({
      inDuration: 300,
      outDuration: 225,
      constrainWidth: false, // Does not change width of dropdown to that of the activator
      hover: false, // Activate on hover
      gutter: 2, // Spacing from edge
      belowOrigin: true, // Displays dropdown below the button
      alignment: 'left', // Displays dropdown with edge aligned to the left of button
      stopPropagation: false // Stops event propagation
    });   
  $(".dropdown-btn").dropdown();
	$('.collapsible').collapsible();

		$('.slider').slider({
    full_width: true,
    interval:5000,
    transition:800,
    });
  
   $('.carousel').carousel({fullWidth: true});

   $('.modal').modal();

   $('.tabs').tabs();
   $('.collapsible').collapsible();

     $('.datepicker').datepicker({
        autoClose:true,
        format:'yyyy-mm-dd',
        minDate:new Date()
    });

    $('.timepicker').timepicker({autoClose:true});
   
    $('.materialboxed').materialbox();
    
  });
