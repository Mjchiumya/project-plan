 $(document).ready(function () {

    var url = window.location.origin;
   $('#users').DataTable({

        "processing": true,
        "serverSide": false,
        "pagingType":"full",
        "dom": '<"toolbar">frtip',
        "type":"POST",
        "ajax": {
          "url":url+'/perfect-plan/adminusers',
          "type":"POST"},

         "columns": [
            { "data": "u_name" },
            { "data": "u_mobile" },
            { "data": "u_email" },
            { "data": "u_status" }
          ] 
      });

    
});